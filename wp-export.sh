#!/bin/bash
wp db --path=wp export --extended-insert=FALSE --skip-dump-date db/etcwebsite.sql
wp export --path=wp --dir=db --filename_format=data.xml
cd wp && wp eval-file ../export-wp-types.php > ../db/types.xml