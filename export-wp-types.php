<?php 
  if (file_exists("wp-content/plugins/types/admin.php")) {
    include_once "wp-content/plugins/types/admin.php";
    include_once "wp-content/plugins/types/includes/import-export.php";    
  } else {
    include_once "wp-content/plugins/types/library/toolset/types/admin.php";
    include_once "wp-content/plugins/types/library/toolset/types/includes/import-export.php";    
  }
  echo wpcf_admin_export_data(false);
?>