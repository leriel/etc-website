var gulp        = require('gulp');
var $           = require('gulp-load-plugins')();
var browserSync = require('browser-sync').create();
var reactify    = require('reactify');
var del         = require('del');
var argv        = require('yargs').argv;
var envify      = require('loose-envify');
var merge       = require('merge-stream');

var flags = {
  production: argv.production,
  target: argv.target == 'static' ? 'static' : 'wp',
};

if (argv.pr) {
  flags = {
    production: true,
    target: 'static',
  }
}

var buildConfig = {
  targetDir: flags.target == 'wp' ? 'wp/wp-content/themes/etccreative' : 'build',
  fontDir: 'fonts',
  styleDir: 'styles',
  imageDir: 'images',
  scriptDir: 'js',
  mediaDir: 'media',
};

process.env.NODE_ENV = flags.production ? 'production' : 'development';

// Static server
gulp.task('watch', ['build'], function() {
  var bsConfig = {
    host: 'etc.mamp',
    proxy: 'etc.mamp',
    port: 8080,
    // server: {
    //   baseDir: buildConfig.targetDir
    // },
    socket: {
      domain: 'etc.mamp:8080',
    },
    open: 'external',
    ghostMode: {
      clicks: false,
    }
  };
  browserSync.init(bsConfig);
  gulp.watch(
    ['theme/**/*.html', 'theme/**/*.php'],
    ['html', browserSync.reload]
  );
  gulp.watch(
    'theme/**/*.scss',
    ['styles', browserSync.reload]
  );
  gulp.watch(
    'theme/images/**/*',
    ['images', browserSync.reload]
  );
  gulp.watch(
    'theme/js/**/*',
    ['scripts', browserSync.reload]
  );
});

gulp.task('clean', function() {
  return del([
    buildConfig.targetDir + '/*',
  ]);
});

gulp.task('styles', function() {
  return gulp.src('theme/styles/main.scss')
    .pipe($.sass({
      outputStyle: flags.production ? 'compressed' : 'nested',
      precision: 10
    }))
    .pipe($.autoprefixer('last 1 version'))
    .pipe(gulp.dest(buildConfig.targetDir + '/' + buildConfig.styleDir))
    .pipe($.size());
});

gulp.task('build', ['html', 'styles', 'scripts', 'images', 'fonts', 'copy', 'vendor']);
gulp.task('default', ['build']);

gulp.task('revision', ['clean', 'build'], function() {
  return gulp.src([
    buildConfig.targetDir + '/**/*.css',
    buildConfig.targetDir + '/**/*.js'
  ])
    .pipe($.rev())
    .pipe(gulp.dest(buildConfig.targetDir))
    .pipe($.rev.manifest())
    .pipe(gulp.dest(buildConfig.targetDir));
});

gulp.task('revreplace', ['revision'], function() {
  var manifest;
  manifest = gulp.src(buildConfig.targetDir + '/rev-manifest.json');

  return gulp.src('theme/index.html')
    .pipe($.revReplace({manifest: manifest}))
    .pipe(gulp.dest(buildConfig.targetDir));
});

gulp.task('html',
  function() {
    return gulp.src(['theme/**/*.html', 'theme/**/*.php'])
      .pipe(gulp.dest(buildConfig.targetDir))
      .pipe($.size());
  });

gulp.task('vendor',
  function() {
    return gulp.src(['theme/vendor/**/'])
    .pipe(gulp.dest(buildConfig.targetDir + '/vendor'))
  });

gulp.task('images', function() {
  var svgFilter = $.filter('**/*.svg', {restore: true});
  
  return gulp.src('theme/images/*')
    .pipe(svgFilter)
    .pipe($.svgmin({
      plugins: [
        {collapseGroups: false}
      ]
    }))
    .pipe(svgFilter.restore)
    .pipe(gulp.dest(buildConfig.targetDir + '/' + buildConfig.imageDir))
    .pipe($.size());
});

gulp.task('fonts', function() {
  return gulp.src([
    'node_modules/bootstrap-sass/assets/fonts/bootstrap/*',
    'theme/fonts/*'
  ])
    .pipe(gulp.dest(buildConfig.targetDir + '/' + buildConfig.fontDir))
    .pipe($.size());
});

gulp.task('scripts', function() {
  var stream = gulp.src('theme/js/app.js')
    .pipe($.browserify({
      fullPaths: !flags.production,
      transform: ['reactify',
        envify.bind(null, {_: 'purge', NODE_ENV: 'production'})],
      extensions: ['.jsx'],
      insertGlobals : true,
      debug : !flags.production
    }));
  var vendorStream = gulp.src('theme/js/vendor.js');
  stream = merge(stream, vendorStream);
  stream = flags.production ? stream.pipe($.uglify()) : stream;
  stream.pipe(gulp.dest(buildConfig.targetDir + '/' + buildConfig.scriptDir))
    .pipe($.size());
  return stream;
});

gulp.task('copy', function() {
  var plainAssets = [
    'theme/favicon.ico',
    'theme/style.css',
    'theme/screenshot.png',
    'theme/instagram.json',
  ];
  gulp.src(plainAssets)
    .pipe(gulp.dest(buildConfig.targetDir))
    .pipe($.size());
});
