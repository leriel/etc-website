var Rsync = require('rsync');
var rsync = new Rsync()
  .shell('ssh')
  .flags('az')
  .progress()
  .delete()
  .source('build/')
  .destination('scaleway:/home/jacek/etc');

rsync.execute(
  (err, code) => {
    console.log(err)
    console.log(code)
  },
  data => console.log(data.toString())
);
