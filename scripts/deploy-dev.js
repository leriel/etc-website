var Rsync = require('rsync');
var rsync = new Rsync()
  .shell('ssh')
  .flags('az')
  .progress()
  .delete()
  .source('build/')
  .destination('scaleway:/home/jacek/etc-dev');

rsync.execute(
  (err, code) => {
    console.log(err)
    console.log(code)
  },
  data => console.log(data.toString())
);
