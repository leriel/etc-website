var React = require('react');
var DocumentTitle = require('react-document-title');

var About = React.createClass({
  render: function() {
    return (
      <DocumentTitle title="About">
        <div className="container-fluid contact-wrapper">
          <section className="contact-left">
            <p>We represent creative visionaries in Photography, Live Action, Motion Animation and Music.</p>
            <p>With offices in New York &amp; Chicago, our artists collaborate with ad agencies, design firms and corporations to create solid branding for their clients.</p>
            <p>We commit that our resources are un-paralleled, that our projects are accomplished on-target, within budget, &amp; that our success is measured on creativity, high-level performance &amp; trust.</p>
            <p>We are thankful to be among the best in our industry, representing award winning talent &amp; having some fun along the way.</p>
          </section>
          <section className="contact-right">
            <p>
              <span className="text-primary">NEW YORK</span>
              <br/>
              347-409-9973
              <br/>
              <span className="text-primary">CHICAGO</span>
              <br/>
              847-563-8178
            </p>
            <p>
              <span className="text-primary">CONTACT</span>
              <br/>
              Principal Agent
              <br/>
              Erica Chadwick
              <br/>
              <a href="mailto:erica@etccreativeinc.com">
                erica@etccreativeinc.com
              </a>
            </p>
          </section>
        </div>
      </DocumentTitle>
    );
  }
});

module.exports = About;
