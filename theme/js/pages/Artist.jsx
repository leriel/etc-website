var React = require('react');
var connect = require('react-redux').connect;
var DocumentTitle = require('react-document-title');
var GridGallery = require('../GridGallery');
var Link = require('react-router').Link;
var _ = require('lodash');
var config = require('../config');
var classNames = require('classnames');

var Artist = React.createClass({
  getInitialState: function() {
    return {
      showBio: false,
    }
  },
  switchBio: function() {
    this.setState({showBio: !this.state.showBio});
  },
  // regular function
  renderCrossHatch: function() {
    var props = this.props;
    var artistSlug = props.params.artistSlug;
    var artist = this.props.artists[artistSlug];
    var videoParam = props.params.videoParam;
    var defs = videoParam ? props.videos : props.galleries;
    var defKeys = Object.keys(defs);
    var data = defs[artistSlug]
    var categoryData = props.categoriesByGallery;
    var catDataLoaded = Object.keys(categoryData).length;
    var showBio = this.state.showBio;
    var leftItems = [];
    var rightItems = [];
    var catList = {};
    var targetBag;
    var gallery;
    var catIndex = catIndex;
    if (defs && defKeys && defKeys.length > 0 && !data) {
      return (
        <h1 className="col-md-12 text-center loading-info">No data!</h1>
      );
    }
    if (!data || !data.length || !categoryData || !catDataLoaded) {
      return (
        <h1 className="col-md-12 text-center loading-info">Loading ...</h1>
      );
    }
    for (var i = 0; i < data.length; i++) {
      gallery = data[i];
      if (!gallery || !gallery.picture) {
        continue;
      }
      if (gallery.position == config.keys.CROSSHATCH_LEFT_POSITION) {
        targetBag = leftItems;
      } else if (gallery.position == config.keys.CROSSHATCH_RIGHT_POSITION) {
        targetBag = rightItems;
      } else {
        targetBag = i < data.length / 2 ? leftItems : rightItems;
      }
      targetBag.push({
        name: gallery.name,
        link: gallery.appLink ? (
          gallery.appLink
        ) : (
          '/' + config.linkSlugs.photogallery + '/' + gallery.slug
        ),
      });
      catList = _.assign(catList, categoryData[gallery.slug]);
    }
    if (!artist.bio) {
      return null;
    }
    return (
      <div className="col-md-12">
        <h2 className="artist-bio--title" onClick={this.switchBio}>
          <span className={showBio ? 'bio' : ''}>BIOGRAPHY</span> / CLIENT LIST
        </h2>
      </div>
    );
  },
  // regular function
  renderGallery: function() {
    var artistSlug = this.props.params.artistSlug;
    var videoParam = this.props.params.videoParam;
    var layout = videoParam ? 'col-sm-6' : '';
    var data = videoParam ? (
      this.props.videos[artistSlug]
    ) : (
      this.props.galleries[artistSlug]
    );
    var galleryData;
    var videos = this.props.videos;
    var hasVideos = videos[artistSlug] && videos[artistSlug].length > 0;
    var categoryData = this.props.categoriesByGallery;
    var catDataLoaded = Object.keys(categoryData).length;
    var categoryFilter = this.props.params.categoryFilter;
    var artistSlug = this.props.params.artistSlug;
    var artist = this.props.artists[artistSlug];
    var ls = config.linkSlugs;
    var videoLink = '/' + ls.artist + '/' + artistSlug + '/' + ls.videos;
    var galleryClass = classNames({
      'col-md-12': true,
      'gallery-maxcols-2': true,
      'subpage-photos': true,
      'video-thumbs': videoParam,
    });
    if (!data || !data.length || !categoryData || !catDataLoaded) {
      return null;
    }
    galleryData = _.clone(data);
    if (hasVideos && !videoParam) {
      galleryData.push({
        name: 'motion',
        appLink: videoLink,
        picture: artist.vpicture,
        width: artist.vwidth,
        ratio: artist.vratio,
      });
    }
    return (
      <GridGallery
        key={artistSlug + videoParam}
        data={galleryData}
        extraData={categoryData}
        linkBase={config.linkSlugs.photogallery}
        columnLayout={layout}
        responsiveClass={galleryClass}
        filter={categoryFilter}
        skipCategories
      />
    );
  },
  // react lifecycle function
  render: function() {
    var artistSlug = this.props.params.artistSlug;
    var artist = this.props.artists[artistSlug];
    var artistName = artist && artist.name ? artist.name : '';
    var videoParam = this.props.params.videoParam;
    var categoryFilter = this.props.params.categoryFilter;
    var categoryText = categoryFilter ? ': ' + categoryFilter : '';
    var baseSlug = config.linkSlugs.artist;
    var headerLink = '/' + baseSlug + '/' + artistSlug;
    var artistGalleries = this.props.galleries[artistSlug];
    if (!artistGalleries || artistGalleries.length <= 0) {
      headerLink += '/' + videoParam;
    }
    var bio = null;
    if (this.state.showBio) {
      bio = (
        <div className="artist-bio--content">
          <div dangerouslySetInnerHTML={{__html: artist.bio}} />
          <a className="breadcrumb-link bio-close" onClick={this.switchBio}>
            X
          </a>
        </div>
      );
    }
    return (
      <DocumentTitle title={artistName}>
        <section className="container-fluid artist-container">
          {bio}
          <div className="row">
            <h1 className="page-title col-md-12">
              <Link
                className="breadcrumb-link"
                activeClassName="breadcrumb-link__active"
                to={headerLink}
              >
                {artistName}
              </Link>
              {categoryText}
            </h1>
          </div>
          <div className="row">
            {this.renderCrossHatch()}
            {this.renderGallery()}
          </div>
        </section>
      </DocumentTitle>
    );
  }
});

module.exports = connect(function(state) {
  var ret = {
    artists: _.get(state, 'sharedData.artistBySlug'),
    galleries: _.get(state, 'sharedData.galleriesByArtist'),
    videos: _.get(state, 'sharedData.videosByArtist'),
    categoriesByGallery: state.sharedData.categoriesByGallery,
  };
  return ret;
})(Artist);
