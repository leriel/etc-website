var React = require('react');
var connect = require('react-redux').connect;
var DocumentTitle = require('react-document-title');
var GridGallery = require('../GridGallery');
var _ = require('lodash');

var config = require('../config');

var profession = config.categorySlugs.photographers;

var Photographers = React.createClass({
  getInitialState: function() {
    return {
      hoverFilter: {},
    }
  },
  // regular function
  renderGallery: function() {
    var data = this.props.data;
    var categoriesByArtist = this.props.categoriesByArtist;
    var categoryFilter = this.props.params.categoryFilter;
    var hoverFilter = this.state.hoverFilter;
    if (!data || !data.length) {
      return null;
    }
    return (
      <GridGallery
        key="photographers"
        data={data}
        preFilter={hoverFilter}
        extraData={categoriesByArtist}
        responsiveClass="col-md-12 gallery-maxcols-2 subpage-photos"
        linkBase={config.linkSlugs.artist}
        filter={categoryFilter}
        skipCategories
      />
    );
  },
  onItemHover: function(item) {
    this.setState({hoverFilter: item})
  },
  onItemHoverOut: function() {
    this.setState({hoverFilter: {}});
  },
  // react lifecycle function
  render: function() {
    return (
      <DocumentTitle title="Photographers">
        <section className="container-fluid">
          <div className="row">
            {this.renderGallery()}
          </div>
        </section>
      </DocumentTitle>
    );
  }
});

module.exports = connect(function(state, ownProps) {
  var artists = _.get(state, 'sharedData.artistsByProfession');
  var data = artists[profession];
  var catData = state.sharedData.categoriesByArtist;
  var cat;
  var ret = {
    data: data,
    artists: _.get(state, 'sharedData.artistsByProfession'),
    categoriesByArtist: catData,
  };
  if (!data) {
    return ret;
  }
  data.forEach(function(artist) {
    if (!artist.videos) {
      return;
    }
    cat = Object.assign({}, catData[artist.slug], {
      motion: {
        index: 0,
        name: 'motion',
        slug: 'motion',
        picture: artist.vpicture || artist.picture,
        ratio: artist.vratio || artist.ratio,
        width: artist.vwidth || artist.width,
      }
    });
    catData = Object.assign({}, catData);
    catData[artist.slug] = cat;
  });
  if (ownProps.params.categoryFilter === 'motion') {
    data = data.map(function(artist) {
      return Object.assign({}, artist, {
        appLink: '/' + config.linkSlugs.artist + '/' + artist.slug + '/' +
          config.linkSlugs.videos
      });
    });
  }
  data = data.map(function(artist) {
    if (!artist.ppicture) {
      return artist;
    }
    return Object.assign({}, artist, {
      picture: artist.ppicture,
      ratio: artist.pratio,
      width: artist.pwidth,
    });
  });
  ret.data = data;
  return ret;
})(Photographers);
