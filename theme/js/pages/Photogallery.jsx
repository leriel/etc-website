var React = require('react');
var Gallery = require('./Gallery');
var connect = require('react-redux').connect;
var GifGrid = require('../GifGrid');
var config = require('../config');

var Photogallery = function(props) {
  var def = props.galleriesBySlug[props.params.slug];
  var gifGallery = false;
  var gifMatch = /\.gif$/;
  function loadData() {
    var url = config.apiBase + config.endpoints.photogallery +
      '&filter[name]=' + props.params.slug;
    var dispatch = props.dispatch;
    return fetch(url)
      .then(function(response) {
        return response.json();
      })
      .then(function(json) {
        dispatch({type: 'SHARED_GALLERY_LOADED', data: json});
      });
  }
  if (!def) {
    loadData();
    return (
      <h1 className="col-md-12 text-center loading-info">
        Loading...
      </h1>
    );
  }
  if (def.images) {
    def.images.forEach(function(imageDef) {
      if (gifGallery) {
        return;
      }
      if (imageDef.picture && imageDef.picture.match(gifMatch)) {
        gifGallery = true;
        return;
      }
      if (imageDef.src && imageDef.src.match(gifMatch)) {
        gifGallery = true;
        return;
      }
    });
  }
  // if (def && def.photo && def.photo.match(gifMatch)) {
  //   gifGallery = true;
  // }
  if (gifGallery) {
    return (
      <GifGrid
        data={def.images}
        slug={props.params.slug}
      />
    );
  }
  var params = Object.assign({}, props, {type: 'photo'});
  return React.createElement(Gallery, params);
}

module.exports = connect(function (state) {
  var ret = {
    galleriesBySlug: state.sharedData.galleriesBySlug,
  }
  return ret;
})(Photogallery);
