var React = require('react');
var connect = require('react-redux').connect;
var DocumentTitle = require('react-document-title');
var GridGallery = require('../GridGallery');
var _ = require('lodash');

var config = require('../config');

var profession = config.categorySlugs.animation_vfx;

var Animation = React.createClass({
  // regular function
  renderGallery: function() {
    var data = this.props.data.map(function(artist) {
      if (!artist.vpicture) {
        return artist;
      }
      return Object.assign({}, artist, {
        picture: artist.vpicture,
        ratio: artist.vratio,
        width: artist.vwidth,
      });
    });
    var categoryData = this.props.videoCategoriesByArtist;
    var categoryFilter = this.props.params.categoryFilter;
    var smallNames = this.props.location.query.smallNames === '1';
    if (!data || !data.length) {
      return null;
    }
    return (
      <GridGallery
        data={data}
        extraData={categoryData}
        columnLayout="col-sm-6"
        responsiveClass="col-md-12 gallery-maxcols-3 subpage-photos"
        smallNames={smallNames}
        linkBase={config.linkSlugs.artist}
        filter={categoryFilter}
        skipCategories
      />
    );
  },
  // react lifecycle function
  render: function() {
    return (
      <DocumentTitle title="Animation">
        <section className="container-fluid">
          <div className="row">
            {this.renderGallery()}
          </div>
        </section>
      </DocumentTitle>
    );
  }
});

module.exports = connect(function(state) {
  var artists = _.get(state, 'sharedData.artistsByProfession');
  var rawData = artists[profession];
  var data = [];
  var ls = config.linkSlugs;
  var ret = {
    artists: _.get(state, 'sharedData.artistsByProfession'),
    data: [],
    videoCategoriesByArtist: state.sharedData.videoCategoriesByArtist,
  };
  if (!rawData) {
    return ret;
  }
  rawData.forEach(function(artist) {
    var artistData = Object.assign({}, artist, {
      appLink: '/' + ls.artist + '/' + artist.slug + '/' + ls.videos
    });
    if (!artist.videos.length) {
      return;
    }
    data.push(artistData);
  });
  data = data.map(function(artist) {
    if (!artist.apicture) {
      return artist;
    }
    return Object.assign({}, artist, {
      picture: artist.apicture,
      ratio: artist.aratio,
      width: artist.awidth,
    });
  });
  ret.data = data;

  return ret;
})(Animation);
