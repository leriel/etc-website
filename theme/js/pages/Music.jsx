var React = require('react');
var connect = require('react-redux').connect;
var DocumentTitle = require('react-document-title');
var GridGallery = require('../GridGallery');
var Link = require('react-router').Link;
var _ = require('lodash');
var classNames = require('classnames');

var config = require('../config');

var profession = config.categorySlugs.music;

var Music = React.createClass({
  // regular function
  renderGallery: function() {
    var data = this.props.data;
    var categoryData = this.props.videoCategoriesByArtist;
    var categoryFilter = this.props.params.categoryFilter;
    var smallNames = this.props.location.query.smallNames === '1';
    var galleryClass = classNames({
      'col-md-12': true,
      'gallery-maxcols-3': true,
      'subpage-photos': true,
      'video-thumbs': true,
    });
    if (!data || !data.length) {
      return null;
    }
    return (
      <GridGallery
        data={data}
        extraData={categoryData}
        columnLayout="col-sm-6"
        responsiveClass={galleryClass}
        smallNames={smallNames}
        linkBase={config.linkSlugs.artist}
        filter={categoryFilter}
        skipCategories
      />
    );
  },
  // react lifecycle function
  render: function() {
    var categoryFilter = this.props.params.categoryFilter;
    var categoryText = categoryFilter ? ': ' + categoryFilter : '';
    return (
      <DocumentTitle title="Music">
        <section className="container-fluid">
          <div className="row">
            <h1 className="page-title col-md-12">
              <Link
                className="breadcrumb-link"
                activeClassName="breadcrumb-link__active"
                to={'/' + config.linkSlugs.music}
              >
                Music
              </Link>
              {categoryText}
            </h1>
          </div>
          <div className="row">
            {this.renderGallery()}
          </div>
        </section>
      </DocumentTitle>
    );
  }
});

module.exports = connect(function(state) {
  var artists = _.get(state, 'sharedData.artistsByProfession');
  var rawData = artists[profession];
  var data = [];
  var ls = config.linkSlugs;
  var ret = {
    artists: _.get(state, 'sharedData.artistsByProfession'),
    data: data,
    videoCategoriesByArtist: state.sharedData.videoCategoriesByArtist,
  };
  if (!rawData) {
    return ret;
  }
  rawData.forEach(function(artist) {
    var artistData = Object.assign({}, artist, {
      appLink: '/' + ls.artist + '/' + artist.slug + '/' + ls.videos
    });
    data.push(artistData);
  });
  data = data.map(function(artist) {
    if (!artist.mpicture) {
      return artist;
    }
    return Object.assign({}, artist, {
      picture: artist.mpicture,
      ratio: artist.mratio,
      width: artist.mwidth,
    });
  });
  ret.data = data;
  return ret;
})(Music);
