var React = require('react');
var Gallery = require('./Gallery');

var Videogallery = function(props) {
  var params = Object.assign({}, props, {type: 'video'});
  return React.createElement(Gallery, params);
}

module.exports = Videogallery;
