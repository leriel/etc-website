var React = require('react');
var connect = require('react-redux').connect;
var DocumentTitle = require('react-document-title');
var BlogFeed = require('../BlogFeed');
var InstagramFeed = require('../InstagramFeed');
var GridGallery = require('../GridGallery');
var config = require('../config');
var _ = require('lodash');

var Homepage = React.createClass({
  // react lifecycle function
  render: function() {
    var horizontal = this.props.location.query.horizontal === '1';
    var data = this.props.data;
    return (
      <DocumentTitle title="Homepage">
        <div>
          <section className="container-fluid">
            <div className="row">
              <GridGallery
                key="homepage"
                horizontal={horizontal}
                data={data}
                linkBase={config.linkSlugs.artist}
                responsiveClass="col-md-12 gallery-maxcols-2"
                skipCategories
              />
            </div>
          </section>
          <section className="container-fluid">
            <div className="row feed-wrapper">
              <BlogFeed/>
              <div className="vr hidden-xs"/>
              <InstagramFeed/>
            </div>
          </section>
        </div>
      </DocumentTitle>
    );
  }
});

module.exports = connect(function(state) {
  var data = _.get(state, 'sharedData.artistsByProfession.all');
  if (data) {
    data = data.map(function(artist) {
      var appLink;
      var ls = config.linkSlugs;
      if (artist.photogalleries.length > 0) {
        return artist;
      }
      appLink = '/' + ls.artist + '/' + artist.slug + '/' + ls.videos;
      return Object.assign({}, artist, {appLink: appLink});
    });
  }
  var ret = {
    data: data,
  };
  return ret;
})(Homepage);
