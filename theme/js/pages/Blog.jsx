var React = require('react');
var BlogFeed = require('../BlogFeed');
var connect = require('react-redux').connect;
var htmlentities = require('he');
var classNames = require('classnames');
var config = require('../config');
var Link = require('react-router').Link;
var hashHistory = require('react-router').hashHistory;

var Blog = React.createClass({
   getInitialState: function() {
    return {
      showPost: true,
    }
  },
  switchPost: function() {
    this.setState({showPost: !this.state.showPost});
  },
  render: function() {
    console.log(this.state.showPost);
    var data = this.props.data;
    var posts = Object.keys(data);
    var postSlug = this.props.params.postSlug;
    var postContent = null;
    /*var className = classNames(
      {'hidden-sm': postSlug},
      {'hidden-xs': postSlug}
    );*/
    var className = classNames(
      {'hidden-sm': this.state.showPost},
      {'hidden-xs': this.state.showPost}
    );
    var closeLink = '/' + config.linkSlugs.blog;
    var post;
    var title;
    if (posts.length > 0 && !postSlug && this.state.showPost) {
      hashHistory.push('/blog/' + posts[0]);
      return null;
    }
    if (postSlug && data && data[postSlug] && this.state.showPost) {
      post = data[postSlug];
      title = htmlentities.decode(post.title);
      postContent = (
        <div className="blogpage-post--content col-md-6">
          <h4 className="blogfeed-posttitle">
            {title}
            {/*<Link
              to={closeLink}
              className="pull-right hidden-md hidden-lg blogpage-post--close"
            >
              X
            </Link>*/}
            <a className="pull-right hidden-md hidden-lg blogpage-post--close" onClick={this.switchPost}>
            X
          </a>
          </h4>
          <div dangerouslySetInnerHTML={{__html: post.content}}></div>
        </div>
      );
    }
    return (
      <div className="container-fluid blog-page">
        <div className="row">
          <BlogFeed
            showTitle={false}
            postSlug={postSlug}
            className={className}
            numPosts={-1}
            blogPage
          />
          {postContent}
        </div>
      </div>
    );
  }
});

module.exports = connect(function(state) {
  return {data: state.sharedData.postBySlug}
})(Blog);
