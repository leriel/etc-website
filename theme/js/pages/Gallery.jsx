var React = require('react');
var DocumentTitle = require('react-document-title');
var connect = require('react-redux').connect;
var Link = require('react-router').Link;
var config = require('../config');
var Slider = require('react-slick');
var _ = require('lodash');
var hashHistory = require('react-router').hashHistory;
var classNames = require('classnames');
var Cookies = require('js-cookie');
var Share = require('../components/Share');
var GridGallery = require('../GridGallery');

var fixSliderTimer;
var gotoTimer;
var Gallery = React.createClass({
  getInitialState: function() {
    return {
      slideIndex: 0,
      fixedSlider: false,
      popupSource: '',
      imagesLoaded: [],
      videosLoaded: [],
      videoIndex: '',
    }
  },
  isVideo: function() {
    var type = this.props.type;
    return type === config.keys.TYPE_VIDEO;
  },
  componentDidMount: function() {
    this.indexById = [];
    var slug = this.props.params.slug;
    var dataPool = this.props.galleriesBySlug;
    var galleryUrl = config.apiBase;
    if (this.isVideo()) {
      if (!this.state.fixedSlider) {
        fixSliderTimer = setTimeout(this.fixSlider, 0);
      }
      return;
    }
    galleryUrl += config.endpoints.photogallery + '&filter[name]=' + slug;
    if (dataPool[slug]) {
      return;
    }
    this.loadData(galleryUrl, this.filterGalleryData, 'SHARED_GALLERY_LOADED');
  },
  // regular function
  loadData: function(url, parser, actionName) {
    var dispatch = this.props.dispatch;
    return fetch(url)
      .then(function(response) {
        return response.json();
      })
      .then(function(json) {
        var targetData = json.map(parser);
        dispatch({type: actionName, data: targetData});
      });
  },
  filterGalleryData: function(gallery) {
    return gallery;
  },
  nextSlide: function() {
    var target = this.state.slideIndex + 1;
    target = target > this.numSlides ? 1 : target;
    this.setState({slideIndex: target});
  },
  prevSlide: function() {
    var target = this.state.slideIndex - 1;
    target = target < -1 ? this.numSlides - 2 : target;
    this.setState({slideIndex: target});
  },
  normalizeIndex: function(index) {
    if (this.numSlides == 0) {
      return 0;
    }
    return index < 0 ? (
      this.numSlides + index
    ) : (
      index % this.numSlides
    );
  },
  denormalizeIndex: function(index) {
    var ret;
    if (this.numSlides == 0) {
      return 0;
    }
    if (this.indexMod !== 0) {
      ret = this.indexMod;
      this.indexMod = 0;
      return ret;
    }
    return index;
  },
  showPopup: function(source) {
    this.setState({popupSource: source});
  },
  showVideo: function(index) {
    this.setState({videoIndex: index});
  },
  hidePopup: function() {
    this.setState({popupSource: ''});
  },
  setIndexMod: function(nextProps) {
    var previousIndex = this.indexById[this.props.params.photoId];
    var nextIndex = this.indexById[nextProps.params.photoId];
    if (previousIndex == 0 && nextIndex >= this.numSlides - 1) {
      this.indexMod = -1;
      return;
    }
    if (previousIndex == this.numSlides - 1 && nextIndex == 0) {
      this.indexMod = this.numSlides;
      return;
    }
  },
  componentWillUpdate: function(nextProps) {
    this.setIndexMod(nextProps);
    var photoId = nextProps.params.photoId;
    var indexById = this.indexById;
    var isVideo = this.isVideo();
    var defs = this.getDefs(nextProps);
    var properId;
    var properSlug;
    var currentIndex;
    var link;
    if (!defs) {
      return;
    }
    var items = isVideo ? defs : defs.images;
    if (!photoId || indexById.length <= 0) {
      return;
    }
    currentIndex = indexById[photoId];
    if (currentIndex > items.length || !items[currentIndex]) {
      link = this.getLink(0);
      hashHistory.push(link);
      return;
    }
    properId = items[currentIndex].id;
    properSlug = items[currentIndex].slug;
    if (photoId !== properId && photoId !== properSlug) {
      link = this.getLink(indexById[photoId]);
      hashHistory.push(link);
    }
  },
  componentWillReceiveProps: function(nextProps) {
    var oldLightbox = this.props.currentLightboxStatus;
    var newLightbox = nextProps.currentLightboxStatus;
    var lightboxData = JSON.stringify(nextProps.lightboxImages);
    if (oldLightbox !== newLightbox) {
      Cookies.set(config.keys.COOKIE_LIGHTBOX, lightboxData);
    }
    if (nextProps.params.photoId != this.props.params.photoId) {
      this.setState({popupSource: '', videoIndex: ''});
    }
  },
  getId: function(index) {
    index = this.normalizeIndex(index);
    var isVideo = this.isVideo();
    var defs = this.getDefs();
    var items = isVideo ? defs : defs.images;
    var id;
    if (!items || !items[index]) {
      return null;
    }
    id = isVideo ? items[index].slug : items[index].id;
    return id;
  },
  getLink: function(index) {
    var id = this.getId(index);
    var photoId = this.props.params.photoId;
    var link = this.props.location.pathname.replace(photoId, id);
    return link;
  },
  pushLink: function(index) {
    var photoId = this.props.params.photoId;
    var id = this.getId(index);
    var link;
    if (photoId === id) {
      return;
    }
    link = this.getLink(index);
    hashHistory.push(link);
  },
  lightboxPress: function() {
    var dispatch = this.props.dispatch;
    var photoId = this.props.params.photoId;
    var defs = this.getDefs();
    var index = this.indexById[photoId];
    var item;
    if (this.isVideo()) {
      return;
    }
    item = defs.images[index];
    item = _.assign({}, item, {picture: item.src});
    dispatch({type: 'LIGHTBOX_SWAP_IMAGE', data: item});
  },
  getDefs: function(source) {
    var props = source || this.props;
    var slug = props.params.slug;
    if (props.data) {
      return props.data;
    }
    var dataPool = this.isVideo() ? (
      props.videosByArtist
    ) : (
      props.galleriesBySlug
    );
    if (!dataPool || !slug) {
      return null;
    }
    return dataPool[slug];
  },
  numSlides: 0,
  slidesToLoad: -1,
  indexMod: 0,
  indexById: [],
  render: function() {
    var props = this.props;
    var state = this.state;
    var type = props.type;
    var isVideo = this.isVideo();
    var artistByGallery = props.artistByGallery;
    var artistBySlug = props.artistBySlug;
    var slideIndex = state.slideIndex;
    var fixedSlider = state.fixedSlider;
    var slug = props.params.slug;
    var lightboxImages = props.lightboxImages
    var defs = this.getDefs();
    var imageLoaded = this.imageLoaded;
    var videoLoaded = this.videoLoaded;
    var pushLink = this.pushLink;
    var photoId = props.params.photoId;
    var photos = [];
    var artistLink = '/';
    var closeLink = '/';
    var popupSource = state.popupSource;
    var imagesLoaded = state.imagesLoaded;
    var videosLoaded = state.videosLoaded;
    var popupElement = null;
    var component = this;
    var artist;
    var artistName;
    var galleryTitle;
    var settings = {
      dots: false,
      arrows: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      variableWidth: true,
      centerMode: true,
      // slickGoTo: slideIndex,
      initialSlide: slideIndex,
    };
    var sliderElement;
    var sliderProps;
    var w;
    var indexById = this.indexById;
    var photoIndex;
    var prevIndex;
    var nextIndex;
    var prevLink;
    var nextLink;
    var getLink = this.getLink;
    var items;
    var linkSuffix;
    var photoElement;
    var lightboxButton;
    var lightboxText;
    var indexOffset = 0;
    var galleryWrapperClass;
    var popupDisplay = 'none';
    if (!defs) {
      return (
        <h1 className="col-md-12 text-center loading-info">
          Loading...
        </h1>
      );
    }
    nextLink = prevLink = '';
    items = isVideo ? defs : defs.images;
    this.numSlides = this.slidesToLoad = items.length;
    artist = isVideo ? slug : artistByGallery[defs.id];
    if (artist) {
      linkSuffix = isVideo ? '/' + config.linkSlugs.videos : '';
      artistLink = '/' + config.linkSlugs.artist + '/' + artist + linkSuffix;
      artistName = artistBySlug[artist].name + ':';
    }
    if (this.props.closeLink) {
      closeLink = this.props.closeLink;
    } else {
      closeLink = artistLink;
    }
    galleryTitle = isVideo ? (
      'videos'
    ) : (
      defs.title.rendered
    );
    galleryTitle = ' ' + galleryTitle;
    if (!photoId && !isVideo) {
      items = items.map(function(item){
        return Object.assign({}, item, {
          appLink: '/' + config.linkSlugs.photogallery + '/' + slug + '/' + item.id
        });
      });
      return (
        <DocumentTitle title={artistName + galleryTitle}>
          <div className="gallery-wrapper container-fluid">
            <div className="row">
              <h1 className="page-title page-title__grid col-md-12">
                <Link
                  className="breadcrumb-link"
                  activeClassName="breadcrumb-link__active"
                  to={artistLink}
                >
                  {artistName}
                </Link>
                <span className="gallery-title">{galleryTitle}</span>
                <Link
                  className="breadcrumb-link close-link pull-right"
                  activeClassName="breadcrumb-link__active"
                  to={closeLink}
                >
                  X
                </Link>
              </h1>
            </div>
            <div className="row">
              <GridGallery
                data={items}
                skipCategories
                linkBase="a"
              />
            </div>
          </div>
        </DocumentTitle>
      );
    }
    if (photoId && indexById.length > 0) {
      photoIndex = indexById[photoId];
      nextIndex = this.normalizeIndex(photoIndex + 1);
      prevIndex = this.normalizeIndex(photoIndex - 1);
      nextLink = this.getLink(nextIndex);
      prevLink = this.getLink(prevIndex);
    }
    items.forEach(function(img, index) {
      var ind = index + indexOffset;
      var className = classNames({
        'slick-image': true,
        'item-picture__loading': !imagesLoaded[ind],
        'item-picture__clickable': img.video,
      });
      var videoPreloader = classNames({
        'slick-image': true,
        // 'item-picture__loading': !imagesLoaded[ind],
        'item-picture__clickable': img.video,
        'video-preloader': !videosLoaded[ind],
      });
      var wrapperClass = classNames({
        'slick-placeholder': true,
        'slick-placeholder__loading': !imagesLoaded[ind]
      });
      var imageSource = (isVideo && img.overlay) ? img.overlay : img.src || img.picture;
      var videoSource;
      var photoOnPress = null;
      var playButton = null;
      w = 'calc(' + (1 / img.ratio) + ' * (100vh - 185px))';
      if (isVideo && !img.video) {
        // indexOffset--;
        // photos.push(<div/>);
        return;
      }
      indexById[img.id] = ind;
      indexById[img.slug] = ind;
      if (img.video) {
        videoSource = img.video.replace(
          '//vimeo.com',
          '//player.vimeo.com/video'
        );
        playButton = (
          <img
            src="images/play-btn.svg"
            className="item-picture--play-button"
            onClick={photoOnPress}
          />
        );
        // photoOnPress = showPopup.bind(component, videoSource);
      }
      photoOnPress = pushLink.bind(component, ind);
      // if (img.video && ind === photoIndex) {
      if (img.video) {
        var zIndex = 0;
        var opacity = 1;
        if (photoIndex === ind) {
          imageSource = '';
        }
        if (photoIndex === ind && videosLoaded[ind]) {
          zIndex = -1;
          opacity = 0;
        }
        photos.push(
          <div
            key={img.id}
            className={wrapperClass}
          >
            <img
              src={imageSource}
              className={videoPreloader}
              onLoad={imageLoaded.bind(component, ind)}
              style={{
                width: w,
                position: 'absolute',
                zIndex: zIndex,
                opacity: opacity
              }}
              onClick={photoOnPress}
            />
            <iframe
              onClick={photoOnPress}
              className="video-frame"
              src={videoSource}
              style={{width: w}}
              onLoad={videoLoaded.bind(component, ind)}
              webkitAllowFullscreen
              mozAllowFullscreen
              allowFullScreen
            />
          </div>
        );
        return;
      }
      photoElement = (
        <div key={img.id} className={wrapperClass}>
          <img
            src={imageSource}
            className={className}
            onLoad={imageLoaded.bind(component, ind)}
            style={{width: w}}
            onClick={photoOnPress}
          />
          {playButton}
        </div>
      );
      photos.push(photoElement);
    });
    // this.numSlides = this.slidesToLoad = photos.length;
      // + ': ' + photoIndex + ': ' + photoId;
    if (photos.length <= 0) {
      sliderElement = (
        <h1 className="col-md-12 text-center loading-info">
          no {type} items ;(
        </h1>
      );
    } else {
      sliderProps = Object.assign({}, settings, {
        children: photos,
        ref: 'slider',
        variableWidth: photos.length >= 3,
        afterChange: function(index) {
          var actualLink = getLink(index);
          var knownLink = getLink(photoIndex);
          if (actualLink === knownLink) {
            return;
          }
          hashHistory.push(actualLink);
        }
      });
      sliderElement = React.createElement(Slider, sliderProps);
    }
    if (popupSource !== '') {
      popupDisplay = '';
    }
    popupElement = (
      <div className="video-popup" style={{display: popupDisplay}}>
        <div className="share-frame">
          <Share key={photoId} onClose={this.hidePopup} download={!isVideo} photoId={photoId}/>
        </div>
      </div>
    );
    lightboxButton = (
      <span className="lightbox-button" style={{visibility: 'hidden'}}>
        +lightbox
      </span>
    );
    if (typeof photoIndex !== 'undefined' && !items[photoIndex].video) {
      lightboxText = lightboxImages[photoId] ? '-lightbox' : '+lightbox';
      lightboxButton = (
        <span
          className="lightbox-button"
          onClick={this.lightboxPress}
        >
          {lightboxText}
        </span>
      );
    }
    galleryWrapperClass = classNames({
      'slick-wrapper': true,
      'slick-wrapper__low-count': photos.length < 3,
    });
    return (
      <DocumentTitle title={artistName + galleryTitle}>
        <div className="gallery-wrapper">
          <h1 className="page-title page-title__gallery">
            <Link
              className="breadcrumb-link"
              activeClassName="breadcrumb-link__active"
              to={artistLink}
            >
              {artistName}
            </Link>
            <span className="gallery-title">{galleryTitle}</span>
            <Link
              className="breadcrumb-link close-link pull-right"
              activeClassName="breadcrumb-link__active"
              to={closeLink}
            >
              X
            </Link>
          </h1>
          <div
            className={galleryWrapperClass}
            style={{opacity: fixedSlider ? 1 : 0}}
          >
            {sliderElement}
            {popupElement}
          </div>
          <div className="slick-controls">
            {lightboxButton}
            <Link
              to={prevLink}
              className="slick-prev-arrow"
            >
              &lt;
            </Link>
            <Link
              to={nextLink}
              className="slick-next-arrow"
            >
              &gt;
            </Link>
            <span
              className="share-button"
              onClick={this.showPopup.bind(this, 'asd')}
            >
              share
            </span>
          </div>
        </div>
      </DocumentTitle>
    );
  },
  componentDidUpdate: function() {
    var defs = this.getDefs();
    var photoId = this.props.params.photoId;
    if (!this.state.fixedSlider) {
      fixSliderTimer = setTimeout(this.fixSlider, 0);
    }
    if (!defs) {
      return;
    }
    if (!photoId && defs.images[0]) {
      // hashHistory.push(this.props.location.pathname + '/' + defs.images[0].id);
      return;
    }
    if (!this.refs.slider) {
      return;
    }
    gotoTimer = setTimeout(this.progGoTo, 0);
  },
  componentWillUnmount: function() {
    fixSliderTimer && clearTimeout(fixSliderTimer);
    gotoTimer && clearTimeout(gotoTimer);
  },
  progGoTo: function() {
    var photoId = this.props.params.photoId;
    var photoIndex = this.indexById[photoId];
    var denormalizedIndex = this.denormalizeIndex(photoIndex);
    if (!this.refs.slider) {
      return;
    }
    this.refs.slider.slickGoTo(denormalizedIndex);
  },
  fixSlider: function() {
    this.setState({fixedSlider: true});
  },
  imageLoaded: function(index) {
    var imagesLoaded = _.assign({}, this.state.imagesLoaded);
    imagesLoaded[index] = true;
    this.setState({imagesLoaded: imagesLoaded});
  },
  videoLoaded: function(index) {
    var videosLoaded = _.assign({}, this.state.videosLoaded);
    videosLoaded[index] = true;
    this.setState({videosLoaded: videosLoaded});
  }

});

module.exports = connect(function(state, ownProps) {
  var lightboxImages = _.get(state, 'sharedData.lightboxImages') || {};
  var photoId = ownProps.params.photoId;
  var slug = ownProps.params.slug;
  var currentLightboxStatus = lightboxImages[photoId];
  var videosByArtist = _.assign({}, state.sharedData.videosByArtist);
  var type = ownProps.type;
  var videoList;
  if (type === config.keys.TYPE_VIDEO && videosByArtist[slug]) {
    videoList = videosByArtist[slug].filter(function(element) {
      return element.video;
    });
    videosByArtist[slug] = videoList;
  }

  var ret = {
    galleriesBySlug: state.sharedData.galleriesBySlug,
    artistByGallery: state.sharedData.artistByGallery,
    artistBySlug: state.sharedData.artistBySlug,
    videosByArtist: videosByArtist,
    lightboxImages: state.sharedData.lightboxImages,
    currentLightboxStatus: currentLightboxStatus,
  };
  return ret;
})(Gallery);
