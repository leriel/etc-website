var React = require('react');
var connect = require('react-redux').connect;
var DocumentTitle = require('react-document-title');
var GridGallery = require('../GridGallery');
var _ = require('lodash');
var config = require('../config');

var Lightbox = React.createClass({
  getInitialState: function() {
    return {
      hoverFilter: {},
    }
  },
  componentDidMount: function() {
    var lightboxImages = this.props.lightboxImages;
    var data = this.props.data;
    var imageKeys = Object.keys(lightboxImages);
    if(imageKeys.length !== data.length) {
      this.loadComponentData();
    }
  },
  componentWillReceiveProps: function(nextProps) {
    var lightboxImages = nextProps.lightboxImages;
    var oldCookie = JSON.stringify(this.props.lightboxImages);
    var newCookie = JSON.stringify(lightboxImages);
    var data = this.props.data;
    var imageKeys = Object.keys(lightboxImages);
    if (oldCookie !== newCookie && imageKeys.length !== data.length) {
      this.loadComponentData();
    }
  },
  loadComponentData: function() {
    var url = config.apiBase + config.endpoints.lightbox;
    this.loadData(url, 'LIGHTBOX_PHOTO_DATA_LOADED');
  },
  loadData: function(url, actionName) {
    var dispatch = this.props.dispatch;
    return fetch(url, {credentials: 'same-origin'})
      .then(function(response) {
        return response.json();
      })
      .then(function(json) {
        dispatch({type: actionName, data: json});
      });
  },
  // regular function
  renderGallery: function() {
    var data = this.props.data;
    if (!data || !data.length) {
      return null;
    }
    return (
      <GridGallery
        data={data}
        responsiveClass="col-md-10 gallery-maxcols-3"
        linkBase={config.linkSlugs.artist}
        skipCategories
      />
    );
  },
  // react lifecycle function
  render: function() {
    var pdfUrl = config.apiBase + config.endpoints.pdf;
    var data = this.props.data;
    if (!data || !data.length) {
      pdfUrl = '#';
    }
    var downloadBtn = <div><a href={pdfUrl}>DOWNLOAD PDF</a></div>;
    return (
      <DocumentTitle title="Lightbox">
        <section className="container-fluid">
          <div className="row">
            <h1 className="page-title col-md-12">
              Lightbox
            </h1>
          </div>
          <div className="row">
            <div className="col-md-2">
              {downloadBtn}
              <div>SEND PDF</div>
              <div>CLEAR LIGHTBOX</div>
            </div>
            {this.renderGallery()}
          </div>
        </section>
      </DocumentTitle>
    );
  }
});

module.exports = connect(function(state) {
  var ret = {
    data: _.get(state, 'sharedData.lightboxData'),
    lightboxImages: _.get(state, 'sharedData.lightboxImages'),
  };
  return ret;
})(Lightbox);
