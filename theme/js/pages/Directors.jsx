var React = require('react');
var connect = require('react-redux').connect;
var DocumentTitle = require('react-document-title');
var GridGallery = require('../GridGallery');
var _ = require('lodash');
var classNames = require('classnames');

var config = require('../config');

var profession = config.categorySlugs.directors;

var Directors = React.createClass({
  // regular function
  renderGallery: function() {
    var data = this.props.data;
    var categoryData = this.props.videoCategoriesByArtist;
    var smallNames = this.props.location.query.smallNames === '1';
    var categoryFilter = this.props.params.categoryFilter;
    var galleryClass = classNames({
      'col-md-12': true,
      'gallery-maxcols-3': true,
      'subpage-photos': true,
      'video-thumbs': true,
    });

    if (!data || !data.length) {
      return null;
    }
    return (
      <GridGallery
        data={data}
        extraData={categoryData}
        columnLayout="col-sm-6"
        responsiveClass={galleryClass}
        smallNames={smallNames}
        linkBase={config.linkSlugs.artist}
        filter={categoryFilter}
        skipCategories
      />
    );
  },
  // react lifecycle function
  render: function() {
    return (
      <DocumentTitle title="Directors">
        <section className="container-fluid">
          <div className="row">
            {this.renderGallery()}
          </div>
        </section>
      </DocumentTitle>
    );
  }
});

module.exports = connect(function(state) {
  var artists = _.get(state, 'sharedData.artistsByProfession');
  var rawData = artists[profession];
  var data = [];
  var ls = config.linkSlugs;
  var ret = {
    artists: artists,
    data: data,
    videoCategoriesByArtist: state.sharedData.videoCategoriesByArtist,
  };
  if (!rawData) {
    return ret;
  }
  rawData.forEach(function(artist) {
    var artistData = Object.assign({}, artist, {
      appLink: '/' + ls.artist + '/' + artist.slug + '/' + ls.videos
    });
    artist.videos.length && data.push(artistData);
  });
  data = data.map(function(artist) {
    if (!artist.dpicture) {
      return artist;
    }
    return Object.assign({}, artist, {
      picture: artist.dpicture,
      ratio: artist.dratio,
      width: artist.dwidth,
    });
  });

  ret.data = data;
  return ret;
})(Directors);
