var React = require('react');
var Gallery = require('./Gallery');
var connect = require('react-redux').connect;
var _ = require('lodash');
var config = require('../config');


var Lightboxgallery = function(props) {
  function loadData(url, actionName) {
    var dispatch = props.dispatch;
    return fetch(url, {credentials: 'same-origin'})
      .then(function(response) {
        return response.json();
      })
      .then(function(json) {
        dispatch({type: actionName, data: json});
      });
  }
  var url = config.apiBase + config.endpoints.lightbox;
  var params = Object.assign({}, props, {
    type: 'photo',
    closeLink: '/' + config.linkSlugs.lightbox,
  });
  if (!props.data || props.data.length <= 0) {
    loadData(url, 'LIGHTBOX_PHOTO_DATA_LOADED');
  }
  return React.createElement(Gallery, params);
}

function mapStateToProps(state) {
  var lightboxData = _.get(state, 'sharedData.lightboxData');
  if (lightboxData.length <= 0) {
    return {
      data: null
    };
  }
  var ret = {
    data: {images: lightboxData, title: {rendered: 'Lightbox'}},
  };
  return ret;
}
module.exports = connect(mapStateToProps)(Lightboxgallery);
