var React = require('react');
var Link = require('react-router').Link;
var Isotope = require('isotope-layout');
var _ = require('lodash');
var htmlentities = require('he');
var classNames = require('classnames');
require('isotope-masonry-horizontal');
require('isotope-packery');

var GridGallery = React.createClass({
  propTypes: {
    data: React.PropTypes.array,
    preFilter: React.PropTypes.object,
    extraData: React.PropTypes.object,
    filter: React.PropTypes.string,
    responsiveClass: React.PropTypes.string,
    columnLayout: React.PropTypes.string,
    horizontal: React.PropTypes.bool,
    smallNames: React.PropTypes.bool,
    stretchImages: React.PropTypes.bool,
    categoriesOnly: React.PropTypes.bool,
    skipCategories: React.PropTypes.bool,
    linkBase: React.PropTypes.string,
    horizontalGrid: React.PropTypes.bool,
  },
  // react lifecycle function
  getDefaultProps: function() {
    var ret = {
      data: [],
      extraData: {},
      filter: '',
      responsiveClass: 'col-md-12 gallery-maxcols-3',
      columnLayout: '',
      horizontal: false,
      smallNames: false,
      stretchImages: true,
      categoriesOnly: false,
      skipCategories: false,
      linkBase: '',
      horizontalGrid: false,
    };
    return ret;
  },
  getInitialState: function() {
    return {
      imagesLoaded: [],
      isotope: false,
    };
  },
  // react lifecycle function
  componentDidMount: function() {
    this.initIsotope();
  },
  renderName: function(item) {
    var linkBase = this.props.linkBase;
    var smallNames = this.props.smallNames;
    var categoriesOnly = this.props.categoriesOnly;
    var address;
    var isGif;
    if (!item || !item.name || categoriesOnly) {
      return null;
    }
    isGif = (typeof item.video !== 'undefined' && !item.appLink);
    if (linkBase && item.slug && !isGif) {
      address = item.appLink ? (
        item.appLink
      ) : (
        '/' + linkBase + '/' + item.slug + '/'
      );
      return (
        <Link
          to={address}
          className={classNames('item-name', {'item-name--small' : smallNames})}
        >
          {item.name}
        </Link>
      );
    }
    return (
      <h1
        className={classNames('item-name', {'item-name--small' : smallNames})}
      >
        {item.name}
      </h1>
    );
  },
  // regular function
  renderItems: function() {
    var data = this.props.data;
    var extraData = this.props.extraData;
    var horizontal = this.props.horizontal;
    var columnLayout = this.props.columnLayout;
    var stretchImages = this.props.stretchImages;
    var categoriesOnly = this.props.categoriesOnly;
    var filter = this.props.filter;
    var preFilter = this.props.preFilter;
    var linkBase = this.props.linkBase;
    var skipCategories = this.props.skipCategories;
    var horizontalGrid = this.props.horizontalGrid;
    var imagesLoaded = this.state.imagesLoaded;
    var linkAddress;
    var items = [];
    var categoryBag = {};
    var categoryKeys = [];
    var categoryElements = [];
    var extraElements = [];
    var itemImage;
    var isImageVertical;
    var catKey;
    var key;
    var item;
    var maxImageWidth;
    var percentRatio;
    var wrapperClassList;
    var imageClassList;
    var catsClassList;
    var showCategories;
    var category;
    var isPreFilter;
    var isPhotoOnly;
    var isGif;
    var picture;
    var width;
    var ratio;
    if (!data || !data.length) {
      return (
        <h1 className="col-md-12 text-center loading-info">Loading ...</h1>
      );
    }
    if (!horizontal && !columnLayout) {
      items.push(
        <div key="grid-sizer" className="grid-sizer"/>
      );
      items.push(
        <div key="gutter-sizer" className="gutter-sizer"/>
      );
    }
    for (var i = 0; i < data.length; i++) {
      isPreFilter = false;
      key = 'g-item-' + i;
      categoryElements = [];
      extraElements = [];
      item = data[i];
      if (!item || !item.picture) {
        continue;
      }
      categoryBag = _.get(extraData, item.slug) || {};
      categoryKeys = Object.keys(categoryBag);
      if (filter && !categoryBag[filter]) {
        continue;
      }
      picture = item.picture;
      ratio = item.ratio;
      width = item.width;
      if (filter && categoryBag[filter].picture) {
        picture = categoryBag[filter].picture;
        ratio = categoryBag[filter].ratio;
        width = categoryBag[filter].width;
      }
      if (preFilter && categoryBag[preFilter.name]) {
        isPreFilter = true;
      }
      showCategories = categoryKeys.length > 0;
      showCategories && !skipCategories && categoryKeys.forEach(function(key) {
        category = categoryBag[key];
        var catText = htmlentities.decode(category.name);
        catKey = key + '-' + category;
        if (category.appLink) {
          categoryElements.push(
            <Link
              to={category.appLink}
              key={catKey}
              className="item-category"
            >
              {catText}
            </Link>
          );
          return;
        }
        categoryElements.push(
          <span key={catKey} className="item-category">{catText}</span>
        );
      });
      if (!showCategories && categoriesOnly) {
        categoryElements.push(
          <span key={catKey} className="item-category"> </span>
        );
      }
      percentRatio = ratio * 100;
      isImageVertical = (ratio > 0.7);
      maxImageWidth = columnLayout || stretchImages ? 'auto' : width;
      catsClassList = classNames(
        'item-categories',
        {'item-categories--small': categoriesOnly}
      );
      !skipCategories && extraElements.push(
        <div key={'item-categories-' + i} className={catsClassList}>
          {categoryElements}
        </div>
      );
      !skipCategories && !categoriesOnly && extraElements.push(
        <hr key={'item-separator' + i} className="item-separator"/>
      );
      isPhotoOnly = ((!item.name || categoriesOnly) && skipCategories);
      imageClassList = classNames(
        'img-responsive',
        'center-block',
        'item-picture',
        {
          'item-picture__loading': !imagesLoaded[i],
          'item-picture__stretched': stretchImages,
        }
      );
      itemImage = (
        <img
          className={imageClassList}
          src={picture}
          onLoad={this.imageLoaded.bind(this, i)}
        />
      );
      isGif = (typeof item.video !== 'undefined' && !item.appLink);
      if (linkBase && (item.slug || item.appLink) && !isGif) {
        linkAddress = item.appLink ? (
          item.appLink
        ) : (
          '/' + linkBase + '/' + item.slug + '/'
        );
        itemImage = (
          <Link to={linkAddress}>{itemImage}</Link>
        );
        if (preFilter && linkAddress === preFilter.link) {
          isPreFilter = true;
        }
      }
      if (!columnLayout) {
        itemImage = (
          <div
            className="item-image--placeholder"
            style={{paddingBottom: percentRatio + '%'}}
          >
            {itemImage}
          </div>
        );
      }
      if (horizontalGrid) {
        itemImage = (
          <img
            src={picture}
          />
        );
        // items.push(itemImage);
        // continue;
      }
      imageClassList = classNames(
        'item-image',
        {
          'item-image--equal-size': columnLayout,
          'item-image--v': columnLayout && isImageVertical,
          'item-image--h': columnLayout &&!isImageVertical,
        }
      );
      wrapperClassList = classNames(
        'text-center',
        {
          'item-wrapper__highlighted': isPreFilter,
          'item-wrapper__photo-only': isPhotoOnly,
          'item-wrapper': !horizontal && !columnLayout && !horizontalGrid,
          'item-wrapper__horizontal': horizontalGrid,
          'item-thumbnail-wrapper': columnLayout,
          'col-lg-3': horizontal && !columnLayout,
          'col-md-4': horizontal && !columnLayout,
          'col-sm-6': horizontal && !columnLayout,
        },
        columnLayout
      );
      items.push(
        <div className={wrapperClassList} key={key}>
          <div
            className={imageClassList}
            style={{maxWidth: maxImageWidth, overflow: 'hidden'}}
          >
            {itemImage}
          </div>
          {this.renderName(item)}
          {extraElements}
        </div>
      );
    }
    return items;
  },
  // react lifecycle function
  render: function() {
    var columnLayout = this.props.columnLayout;
    var horizontal = this.props.horizontal;
    var horizontalGrid = this.props.horizontalGrid;
    var mainClass = classNames(
      this.props.responsiveClass,
      'mainpage-photos',
      {'horizontal-photos' : horizontal}
    );
    var style = {};
    if (columnLayout) {
      style.paddingRight = 0;
    }
    if (horizontalGrid) {
      // style.height = 1500;
      style.width = '75%';
    }
    return (
      <div className={mainClass} style={style}>
        {this.renderItems()}
      </div>
    );
  },
  // react lifecycle function
  componentDidUpdate: function() {
    this.initIsotope();
  },
  // regular function
  initIsotope: function() {
    var data = this.props.data;
    var isoConfig = {
      itemSelector: '.item-wrapper',
      masonry: {
        gutter: '.gutter-sizer',
        columnWidth: '.grid-sizer',
        // fitWidth: true,
      }
    }
    var horizontalGrid = this.props.horizontalGrid;
    if (this.props.horizontal || this.props.columnLayout) {
      return;
    }
    if (this.state.isotope) {
      return;
      // this.iso.destroy();
    }
    if (!data || data.length <= 0) {
      return;
    }
    if (horizontalGrid) {
      // isoConfig = {
      //   layoutMode: 'masonryHorizontal',
      //   itemSelector: '.item-wrapper__horizontal',
      //   masonryHorizontal: {
      //     rowHeight: 300,
      //   },
      // }

      isoConfig = {
        layoutMode: 'fitRows',
        itemSelector: '.item-wrapper__horizontal',
      }

      // isoConfig = {
      //   layoutMode: 'packery',
      //   percentPosition: true,
      //   itemSelector: '.item-wrapper__horizontal',
      // }

      // isoConfig.fitRows = {
      //   // gutter: 10,
      // }
      // return;
    }
    this.setState({isotope: new Isotope('.mainpage-photos', isoConfig)});
  },
  // regular function
  imageLoaded: function(index) {
    var imagesLoaded = _.assign({}, this.state.imagesLoaded);
    imagesLoaded[index] = true;
    this.setState({imagesLoaded: imagesLoaded});
  }
});

module.exports = GridGallery;
