var React = require('react');
var GridGallery = require('./GridGallery');
var Link = require('react-router').Link;
var DocumentTitle = require('react-document-title');
var connect = require('react-redux').connect;
var config = require('./config');

var GifGrid = React.createClass({
  propTypes: {
    data: React.PropTypes.array.isRequired,
  },
  getArtistName: function() {
    var artistBySlug = this.props.artistBySlug;
    var slug = this.props.slug;
    var defs = this.props.galleriesBySlug[slug];
    var artistSlug = this.props.artistByGallery[defs.id];
    var artistName;
    if (!artistBySlug[artistSlug]) {
      return ':';
    }
    artistName = artistBySlug[artistSlug].name + ':';
    return artistName;
  },
  render: function() {
    var data = this.props.data;
    var slug = this.props.slug;
    var defs = this.props.galleriesBySlug[slug];
    var artistSlug = this.props.artistByGallery[defs.id];
    var headerLink = '/' + config.linkSlugs.artist + '/' + artistSlug;
    var artistName = this.getArtistName();
    var galleryTitle = ' ' + defs.title.rendered;
    return (
      <DocumentTitle title="Artist">
        <section className="container-fluid">
          <div className="row">
            <h1 className="page-title page-title__grid col-md-12">
              <Link
                className="breadcrumb-link"
                activeClassName="breadcrumb-link__active"
                to={headerLink}
              >
                {artistName}
              </Link>
              <span className="gallery-title">{galleryTitle}</span>
              <Link
                className="breadcrumb-link close-link pull-right"
                activeClassName="breadcrumb-link__active"
                to={headerLink}
              >
                X
              </Link>
            </h1>
          </div>
          <div className="row">
            <GridGallery
              data={data}
              skipCategories
            />
          </div>
        </section>
      </DocumentTitle>
    );
  }
});

module.exports = connect(function (state) {
  return {
    galleriesBySlug: state.sharedData.galleriesBySlug,
    artistByGallery: state.sharedData.artistByGallery,
    artistBySlug: state.sharedData.artistBySlug,
  };
})(GifGrid);
