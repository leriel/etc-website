var React = require('react');
var Main = require('./Main');

var MainClean = function(props) {
  var params = Object.assign({}, props, {noMenu: true});
  return React.createElement(Main, params);
}

module.exports = MainClean;
