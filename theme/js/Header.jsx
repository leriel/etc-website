var React = require('react');
var Link = require('react-router').Link;
var classNames = require('classnames');

var Header = React.createClass({
  getInitialState: function() {
    return {
      shrinked: false,
      mobileMenu: false,
    };
  },
  /*componentWillMount: function() {
    this.debouncedScroll = _.debounce(this.handleScroll, 10);
  },
  componentDidMount: function() {
    window.addEventListener('scroll', this.debouncedScroll);
  },
  componentWillUnmount: function() {
    window.removeEventListener('scroll', this.debouncedScroll);
  },*/
  toggleNav: function(event) {
    event.stopPropagation();
    this.setState({mobileMenu: !this.state.mobileMenu});
  },
  hideNav: function(event) {
    event.stopPropagation();
    this.setState({mobileMenu: false});
  },
  handleClick: function() {
    // this.setState({shrinked: !this.state.shrinked});
  },
 /* handleScroll: function(event) {
    if (!this.state.shrinked && window.pageYOffset > 0) {
      this.setState({shrinked: true});
    } else if (this.state.shrinked && window.pageYOffset <= 0) {
      this.setState({shrinked: false});
    }
  },*/
  render: function() {
    var mobileMenu = this.state.mobileMenu;
    var mobileMenuClass = classNames(
      'navbar-collapse',
      'navbar-right',
      'cl',
      {'in': mobileMenu}
    );
    return (
      <nav
        className={classNames('navbar')}
        role="navigation"
        onClick={this.handleClick}
      >
        <div className="container-fluid menu-wrapper">
          <div className="navbar-header">
            <button
              type="button"
              className="navbar-toggle"
              onClick={this.toggleNav}
            >
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <Link className="pull-left" to="/">
              <img
                className="main-logo"
                src="/images/logo.svg"
                alt="ETC Creative"
              />
            </Link>
          </div>
          <div className={mobileMenuClass} aria-expanded="false">
            <ul className="nav navbar-nav">
              <li className="menu-item">
                <Link
                  to="/photographers"
                  activeClassName="current-page"
                  onClick={this.hideNav}
                >
                  Photographers
                </Link>
              </li>
              <li className="menu-item">
                <Link
                  to="/directors"
                  activeClassName="current-page"
                  onClick={this.hideNav}
                >
                  Directors
                </Link>
              </li>
              <li className="menu-item">
                <Link
                  to="/artist/spacejunk/videos"
                  activeClassName="current-page"
                  onClick={this.hideNav}
                >
                  Animation + VFX
                </Link>
              </li>
{/*
              <li className="menu-item">
                <Link to="/music" activeClassName="current-page">
                  Music
                </Link>
              </li>
*/}
              <li className="menu-item">
                <Link
                  to="/about"
                  activeClassName="current-page"
                  onClick={this.hideNav}
                >
                  About
                </Link>
              </li>
              <li className="menu-item">
                <Link
                  to="/blog"
                  activeClassName="current-page"
                  onClick={this.hideNav}
                >
                  Blog
                </Link>
              </li>
              <li className="menu-item">
                <Link
                  to="/lightbox"
                  activeClassName="current-page"
                  onClick={this.hideNav}
                >
                  Lightbox
                </Link>
              </li>
              <li className="menu-item menu-icons">
                <a
                  className="menu-icon"
                  href="https://twitter.com/etccreative/"
                  target="_blank"
                >
                  <svg><use xlinkHref="#twitter"></use></svg>
                </a>
                <a
                  className="menu-icon"
                  href="https://www.pinterest.com/etc_creative/"
                  target="_blank"
                >
                  <svg><use xlinkHref="#pinterest"></use></svg>
                </a>
                <a
                  className="menu-icon"
                  href="https://www.instagram.com/thisisetccreative/"
                  target="_blank"
                >
                  <svg><use xlinkHref="#instagram"></use></svg>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="header-separator">
          <div className="separator-bar bar1"/>
          <div className="separator-bar bar2"/>
          <div className="separator-bar bar3"/>
          <div className="separator-bar bar4"/>
          <div className="separator-bar bar5"/>
          <div className="separator-bar bar6"/>
          <div className="separator-bar bar7"/>
          <div className="separator-bar bar8"/>
          <div className="separator-bar bar9"/>
          <div className="separator-bar bar10"/>
          <div className="separator-bar bar11"/>
        </div>
      </nav>
    );
  }
});

module.exports = Header;
