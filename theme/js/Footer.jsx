var React = require('react');

var Footer = React.createClass({
  render: function() {
    return (
      <section className="footer">
        <div className="footer-separator">
          <div className="separator-bar bar1"/>
          <div className="separator-bar bar2"/>
          <div className="separator-bar bar3"/>
          <div className="separator-bar bar4"/>
          <div className="separator-bar bar5"/>
          <div className="separator-bar bar6"/>
          <div className="separator-bar bar7"/>
          <div className="separator-bar bar8"/>
          <div className="separator-bar bar9"/>
          <div className="separator-bar bar10"/>
          <div className="separator-bar bar11"/>
        </div>
        <div className="container-fluid">
          <p className="text-center">ETC Creative represents creative visionaries in photography live action. motion animation and music. We offer full service production support in the space where art and commerce co-exist.</p>
          <p className="text-center">With offices in New York and Chicago, we collaborate with ad agencies, design firms and corporations to create solid branding for their clients.</p>
          <div className="row contact-info text-center">
            <div className="col-sm-4 left-contact">
              <p>
                <span className="text-primary">NEW YORK</span>
                <br/><a href="tel:347-409-9973">347-409-9973</a>
                <br/><span className="text-primary">CHICAGO</span>
                <br/><a href="tel:847-563-8178">847-563-8178</a>
              </p>
            </div>
            <div className="col-sm-4">
              <img className="logo-footer" src="/images/logo-footer.svg"/>
              <div className="footer-socialicons">
                <a
                  className="footer-icon"
                  href="https://twitter.com/etccreative/" target="_blank"
                >
                  <svg><use xlinkHref="#twitter"></use></svg>
                </a>
                <a
                  className="footer-icon"
                  href="https://www.pinterest.com/etc_creative/"
                  target="_blank"
                >
                  <svg><use xlinkHref="#pinterest"></use></svg>
                </a>
                <a
                  className="footer-icon"
                  href="https://www.instagram.com/thisisetccreative/"
                  target="_blank"
                >
                  <svg><use xlinkHref="#instagram"></use></svg>
                </a>
              </div>

            </div>
            <div className="col-sm-4 right-contact">
              <p>
                <span className="text-primary">CONTACT</span>
                <br/>Principal Agent
                <br/>Erica Chadwick
                <br/><a href="mailto:erica@etccreativeinc.com">erica@etccreativeinc.com</a>
              </p>
            </div>
          </div>
        </div>
      </section>
      );
  }
});

module.exports = Footer;
