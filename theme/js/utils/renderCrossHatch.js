var React = require('react');
var CrossHatch = require('../components/CrossHatch');
var _ = require('lodash');
var htmlentities = require('he');

function renderCrossHatch(
  data,
  categoryData,
  itemSlug,
  categorySlug,
  elastic,
  onHover,
  onHoverOut
) {
  var catDataLoaded = Object.keys(categoryData).length;
  var catList = {};
  var categoryKeys = [];
  var category;
  var categoryName;
  var artist;
  var leftItems = [];
  var rightItems = [];
  if (!data || !data.length || !categoryData || !catDataLoaded) {
    return (
      <h1 className="col-md-12 text-center loading-info">
        Loading artists...
      </h1>
    );
  }
  for (var i = 0; i < data.length; i++) {
    artist = data[i];
    catList = _.assign(catList, categoryData[artist.slug]);
    var link = artist.appLink ? (
      artist.appLink
    ) : (
      '/' + itemSlug + '/' + artist.slug + '/'
    );
    leftItems.push({
      name: artist.name,
      link: link,
    });
  }
  categoryKeys = Object.keys(catList);
  for (var i = 0; i < categoryKeys.length; i++) {
    category = catList[categoryKeys[i]];
    categoryName = htmlentities.decode(category.name);
    rightItems.push({
      name: categoryName,
      link: '/' + categorySlug + '/' + category.slug + '/',
      index: category.index,
    });
  }
  rightItems.sort(function(a, b) {return a.index - b.index});
  return (
    <CrossHatch
      leftItems={leftItems}
      rightItems={rightItems}
      elastic={elastic}
      onItemHover={onHover}
      onItemHoverOut={onHoverOut}
    />
  );
}

module.exports = renderCrossHatch;
