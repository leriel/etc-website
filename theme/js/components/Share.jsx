var React = require('react');
var connect = require('react-redux').connect;
var config = require('../config');
var jQuery = require('jquery');

var Share = React.createClass({
  // react lifecycle function
  componentDidMount: function() {
    var contactFormUrl = config.apiBase + config.endpoints.contactform;
    require(
      '../../../wp/wp-content/plugins/contact-form-7/includes/js/jquery.form.js'
    );
    require(
      '../../../wp/wp-content/plugins/contact-form-7/includes/js/scripts.js'
    );
    if (this.props.contactForm) {
      this.forceUpdate();
      return;
    }
    this.loadData(
      contactFormUrl,
      'SHARED_CONTACTFORM_LOADED'
    );
  },
  // regular function
  loadData: function(url, actionName) {
    var dispatch = this.props.dispatch;
    return fetch(url)
      .then(function(response) {
        return response.json();
      })
      .then(function(json) {
        var targetData = json.replace(/\\/g, '');
        dispatch({type: actionName, data: targetData});
      });
  },
  closePress: function() {
    var onClose = this.props.onClose;
    if (typeof onClose !== 'function') {
      return;
    }
    onClose();
  },
  // react lifecycle function
  render: function() {
    var fbUrl = config.shareUrl.fb + encodeURIComponent(location.href);
    var twUrl = config.shareUrl.twitter + encodeURIComponent(location.href);
    var liUrl = config.shareUrl.linkedin + encodeURIComponent(location.href);
    var tuUrl = config.shareUrl.tumblr + encodeURIComponent(location.href);
    var piUrl = config.shareUrl.pinterest + encodeURIComponent(location.href);
    var downloadUrl = config.apiBase + config.endpoints.download + this.props.photoId;
    var downloadBtn = null;
    if (this.props.download) {
      downloadBtn = <a href={downloadUrl}>download</a>;
    }
    return (
      <section className="share-form">
        <div className="share-form--links">
          {downloadBtn}
          <a href={fbUrl} target="_blank">facebook</a>
          <a href={piUrl} target="_blank">pinterest</a>
          <a href={liUrl} target="_blank">lnkedin</a>
          <a href={twUrl} target="_blank">twitter</a>
          <a href={tuUrl} target="_blank">tumblr</a>
        </div>
        <div className="share-form--content">
          <h3 className="share-form--title">
            email
            <span
              className="share-form--close"
              onClick={this.closePress}
            >
              x
            </span>
          </h3>
          <div dangerouslySetInnerHTML={{__html: this.props.contactForm}}></div>
        </div>
      </section>
    );
  },
  initForm: function() {
    jQuery('div.wpcf7 > form').wpcf7InitForm();
    (function(d) {
      var j = d.createElement('script');
      j.src = 'https://www.google.com/recaptcha/api.js?onload=recaptchaCallback&render=explicit&ver=2.0';
      d.body.appendChild(j);
    }(document));
  },
  // react lifecycle function
  componentDidUpdate: function() {
    this.initForm();
  },
});

module.exports = connect(function(state) {
  var ret = {
    contactForm: state.sharedData.contactForm,
  };
  return ret;
})(Share);
