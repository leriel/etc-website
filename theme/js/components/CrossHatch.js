var React = require('react');
var classNames = require('classnames');
var Link = require('react-router').Link;

var CrossHatch = React.createClass({
  propTypes: {
    leftItems: React.PropTypes.array,
    rightItems: React.PropTypes.array,
    elastic: React.PropTypes.bool,
    onItemHover: React.PropTypes.func,
    onItemHoverOut: React.PropTypes.func,
  },
  render: function() {
    var leftItems = this.props.leftItems;
    var rightItems = this.props.rightItems;
    var elastic = this.props.elastic;
    var children = this.props.children;
    var onItemHover = this.props.onItemHover;
    var onItemHoverOut = this.props.onItemHoverOut;
    var hatchLinks = [];

    leftItems.forEach(function(item) {
      var classList = classNames(
        'col-md-6',
        'cross-hatch--artist',
        'text-right',
        {'cross-hatch__elastic': elastic}
      );
      var key = 'an-' + item.name;
      var onHover = onItemHover && onItemHover.bind(null, item);
      hatchLinks.push(
        <div className="row" key={key}>
          <span className={classList}>
            <Link
              to={item.link}
              className="cross-hatch--link"
              activeClassName="cross-hatch--active-link"
              onMouseOver={onHover}
              onMouseOut={onItemHoverOut}
            >
              {item.name}
            </Link>
          </span>
        </div>
      );
    });
    hatchLinks.push(
      <span
        key="hatch-left-separator"
        className="col-md-6 cross-hatch--bottom-separator"
      />
    );
    hatchLinks.push(
      <div key="hatch-separator" className="cross-hatch--separator"/>
    );
    if (rightItems.length) {
      hatchLinks.push(
        <span
          key="hatch-right-separator"
          className="col-md-offset-6 col-md-6 cross-hatch--top-separator"
        />
      );
    }
    rightItems.forEach(function(item) {
      var classList = classNames(
        'col-md-offset-6',
        'col-md-6',
        'cross-hatch--category',
        {'cross-hatch__elastic': elastic}
      );
      var key = 'cn-' + item.name;
      var onHover = onItemHover && onItemHover.bind(null, item);
      hatchLinks.push(
        <span key={key} className={classList}>
          <Link
            to={item.link}
            className="cross-hatch--link"
            activeClassName="cross-hatch--active-link"
            onMouseOver={onHover}
            onMouseOut={onItemHoverOut}
          >
            {item.name}
          </Link>
        </span>
      );
    });
    return (
      <div className="col-md-4 cross-hatch">{hatchLinks}{children}</div>
    );
  }
});

module.exports = CrossHatch;
