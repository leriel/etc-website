var React = require('react');
var SvgImages = require('./SvgImages');
var Header = require('./Header');
var Footer = require('./Footer');
var config = require('./config');
var connect = require('react-redux').connect;
var Cookies = require('js-cookie');
var _ = require('lodash');

var Main = React.createClass({
  // react lifecycle function
  getDefaultProps: function() {
    return {
      noMenu: false,
    }
  },
  // react lifecycle function
  componentDidMount: function() {
    var artistsUrl = config.apiBase + config.endpoints.artists;
    var leftCategoriesUrl = config.apiBase + config.endpoints.lh_categories;
    var rightCategoriesUrl = config.apiBase + config.endpoints.rh_categories;
    var galleryUrl = config.apiBase + config.endpoints.allgalleries;
    var component = this;
    var lightboxData = Cookies.get(config.keys.COOKIE_LIGHTBOX) || '{}';
    lightboxData = JSON.parse(lightboxData);
    this.props.dispatch({type: 'LIGHTBOX_DATA_LOADED', data: lightboxData});
    this.loadData(
      artistsUrl,
      this.filterArtistInfo,
      'HOMEPAGE_ARTISTS_DATA_LOADED'
      )
      .then(function() {
        component.loadData(
          leftCategoriesUrl,
          component.filterCategoryInfo,
          'SHARED_CATEGORIES_DATA_LOADED'
        );
        component.loadData(
          rightCategoriesUrl,
          component.filterCategoryInfo,
          'SHARED_CATEGORIES_DATA_LOADED'
        )
        .then(function() {
          var delay = component.props.params.slug ? 1000 : 0;
          return setTimeout(
            component.loadData.bind(
              component,
              galleryUrl,
              component.filterGalleryData,
              'SHARED_GALLERY_LOADED'
            ),
            delay
          );
        });
      });
  },
  // regular function
  loadData: function(url, parser, actionName) {
    var dispatch = this.props.dispatch;
    return fetch(url)
      .then(function(response) {
        return response.json();
      })
      .then(function(json) {
        var targetData = json.map(parser);
        dispatch({type: actionName, data: targetData});
      });
  },
  // regular function
  filterArtistInfo: function(artist) {
    var ret = {
      id: artist.id,
      name: artist.title.rendered,
      slug: artist.slug,
      picture: artist['profile-picture'].url,
      ratio: artist['profile-picture'].ratio,
      width: artist['profile-picture'].width,
      vpicture: _.get(artist, '["videos-picture"].url'),
      vratio: _.get(artist, '["videos-picture"].ratio'),
      vwidth: _.get(artist, '["videos-picture"].width'),
      ppicture: _.get(artist, '["photographer-picture"].url'),
      pratio: _.get(artist, '["photographer-picture"].ratio'),
      pwidth: _.get(artist, '["photographer-picture"].width'),
      dpicture: _.get(artist, '["director-picture"].url'),
      dratio: _.get(artist, '["director-picture"].ratio'),
      dwidth: _.get(artist, '["director-picture"].width'),
      apicture: _.get(artist, '["animation-picture"].url'),
      aratio: _.get(artist, '["animation-picture"].ratio'),
      awidth: _.get(artist, '["animation-picture"].width'),
      mpicture: _.get(artist, '["music-picture"].url'),
      mratio: _.get(artist, '"music-picture"].ratio'),
      mwidth: _.get(artist, '["music-picture"].width'),
      photogalleries: artist.photogalleries,
      videogalleries: artist.videogalleries,
      videos: artist.videos,
      professions: artist.professions,
      bio: artist.bio,
    };
    return ret;
  },
  // regular function
  filterCategoryInfo: function(category) {
    var ret = {
      name: category.name,
      id: category.id,
      photogalleries: category.photogalleries,
      videogalleries: category.videogalleries,
      taxonomy: category.taxonomy,
      slug: category.slug,
      videos: category.videos,
    };
    return ret;
  },
  filterGalleryData: function(gallery) {
    return gallery;
  },
  // react lifecycle function
  render: function() {
    var noMenu = this.props.noMenu;
    var header = noMenu ? null : <Header/>;
    var footer = noMenu ? null : <Footer/>;
    var mainClass = noMenu ? 'main-wrapper' : 'main-wrapper main-wrapper__menu';
    return (
      <div className={mainClass}>
        <SvgImages/>
        {header}
        {this.props.children}
        {footer}
      </div>
    );
  },
});

module.exports = connect(function(state) {return state;})(Main);
