var Router = require('react-router').Router;
var Route = require('react-router').Route;
var React = require('react');
var PropTypes = require('react').PropTypes;

var Main = require('./Main');
var MainClean = require('./MainClean');
var Homepage = require('./pages/Homepage');
var Photographers = require('./pages/Photographers');
var Directors = require('./pages/Directors');
var Animation = require('./pages/Animation');
var Music = require('./pages/Music');
var About = require('./pages/About');
var Lightbox = require('./pages/Lightbox');
var Artist = require('./pages/Artist');
var Blog = require('./pages/Blog');
var Photogallery = require('./pages/Photogallery');
var Videogallery = require('./pages/Videogallery');
var Lightboxgallery = require('./pages/Lightboxgallery');
var Provider = require('react-redux').Provider;

var Root = React.createClass({
  propTypes: {
    history: PropTypes.object.isRequired
  },
  render: function() {
    var history = this.props.history;
    var store = this.props.store;
    return (
      <Provider store={store}>
        <Router history={history} onUpdate={function() {window.scrollTo(0, 0)}}>
          <Route name="clean" component={MainClean}>
            <Route
              path="/photogallery/:slug(/:photoId)"
              component={Photogallery}
            />
            <Route
              path="/videogallery/:slug(/:photoId)"
              component={Videogallery}
            />
            <Route
              path="/artist/:slug/videos/:photoId"
              component={Videogallery}
            />
            <Route path="/lightbox/:photoId" component={Lightboxgallery}/>
          </Route>
          <Route name="home" component={Main}>
            <Route path="/" component={Homepage}/>
            <Route
              path="/photographers(/:categoryFilter)"
              component={Photographers}
            />
            <Route
              path="/directors(/:categoryFilter)"
              component={Directors}
            />
            <Route
              path="/animation-vfx(/:categoryFilter)"
              component={Animation}
            />
            <Route path="/music(/:categoryFilter)" component={Music}/>
            <Route path="/about" component={About}/>
            <Route path="/lightbox" component={Lightbox}/>
            <Route
              path="/artist/:artistSlug"
              component={Artist}
            >
              <Route path=":videoParam" />
            </Route>
            <Route path="/blog" component={Blog}>
              <Route path=":postSlug" />
            </Route>
          </Route>
        </Router>
      </Provider>
    );
  }
});

module.exports = Root;
