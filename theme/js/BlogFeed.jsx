var React = require('react');
var connect = require('react-redux').connect;
var config = require('./config');
var htmlentities = require('he');
var Link = require('react-router').Link;
var classNames = require('classnames');

var BlogFeed = React.createClass({
  getDefaultProps: function() {
    return {
      showTitle: true,
      blogPage: false,
      numPosts: 6,
    }
  },
  // react lifecycle function
  componentDidMount: function() {
    fetch(config.apiBase + config.endpoints.posts + '&filter[posts_per_page]=' + this.props.numPosts)
      .then(this.onDataLoaded);
  },
  // regular function
  onDataLoaded: function(response) {
    response.json().then(this.onJsonParsed);
  },
  // regular function
  onJsonParsed: function(json) {
    var targetData = json.map(this.filterPostInfo);
    this.props.dispatch({type: 'HOMEPAGE_POSTS_DATA_LOADED', data: targetData});
  },
  // regular function
  filterPostInfo: function(post) {
    var ret = {
      id: post.id,
      title: post.title.rendered,
      homePicture: post.homePicture,
      excerpt: post.excerpt.rendered,
      artist: post.artist,
      slug: post.slug,
      content: post.content.rendered,
    };
    return ret;
  },
  renderPosts: function() {
    // return;
    var data = this.props.data;
    var blogPage = this.props.blogPage;
    var className = classNames(
      'row',
      'blogfeed-post',
      {'blogpage-post': blogPage}
    );
    var posts = [];
    var key;
    var post;
    var title;
    var artistName;
    var link;
    for (var i = 0; i < data.length; i++) {
      post = data[i];
      key = 'post-' + post.id;
      artistName = post.artist ? post.artist.name + ':' : '';
      title = htmlentities.decode(post.title);
      link = '/' + config.linkSlugs.blog + '/' + post.slug;
      posts.push(
        <Link
          to={link}
          key={key}
          className={className}
          activeClassName="blogpage-post__active"
        >
          <div className="col-xs-5 blogfeed-image">
            <img className="img-responsive" src={post.homePicture}/>
          </div>
          <div className="col-xs-7 blogfeed-content">
            <h4 className="blogfeed-artistname">{artistName}</h4>
            <h4 className="blogfeed-posttitle">{title}</h4>
            <div dangerouslySetInnerHTML={{__html: post.excerpt}}></div>
          </div>
        </Link>
        );
    }
    return posts;
  },
  // react lifecycle function
  render: function() {
    var showTitle = this.props.showTitle;
    var className = this.props.className;
    var titleElement = showTitle ? <h1>BLOG FEED</h1> : null;
    var cn = classNames('col-md-6', className, {'custom-scroll': this.props.blogPage});
    return (
      <div className={cn}>
        {titleElement}
        {this.renderPosts()}
      </div>
    );
  }
});

module.exports = connect(function(state) {
  return {data: state.homepageData.postFeed}
})(BlogFeed);
