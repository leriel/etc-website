var React = require('react');
var config = require('./config');
var connect = require('react-redux').connect;
var classNames = require('classnames');
var _ = require('lodash');

var InstagramFeed = React.createClass({
  // react lifecycle function
  componentDidMount: function() {
    var feedUrl = config.apiBase + config.endpoints.instagram +
      config.instagram.postsInFeed;
    fetch(feedUrl)
      .then(this.onDataLoaded);
  },
  // regular function
  onDataLoaded: function(response) {
    response.json().then(this.onJsonParsed);
  },
  // regular function
  onJsonParsed: function(json) {
    if (!json.data) {
      return null;
    }
    var targetData = json.data.map(this.filterPostInfo);
    this.props.dispatch({
      type: 'HOMEPAGE_INSTAGRAM_DATA_LOADED', data: targetData
    });
  },
  // regular function
  filterPostInfo: function(post) {
    var postText = _.get(post, 'caption.text');
    var maxLength = config.instagram.maxPostLength;
    var overMax = false;

    if (postText) {
      overMax = postText.length > maxLength;
      postText = postText.substr(0, maxLength);
      postText = postText.replace(/ [^ ]*$/, '') + (overMax ? '...' : '');
    }
    var ret = {
      link: post.link,
      image: _.get(post, 'images.low_resolution.url'),
      text: postText,
    };
    return ret;
  },
  renderPosts: function() {
    var data = this.props.data;
    var posts = [];
    var key;
    var post;
    var oddRow;
    var smallShift;
    var mediumShift;
    var largeShift;
    var wrapperClasses;
    var className;

    for (var i = 0; i < data.length; i++) {
      oddRow = (i % 7 === 0 || i % 7 === 1 || i % 7 === 2);
      smallShift = (i % 7 === 4);
      mediumShift = (i % 7 === 1);
      largeShift = (i % 7 === 5);
      key = 'insta-post-' + i;
      45, 60, 70
      post = data[i];
      wrapperClasses = {
        'col-sm-4': oddRow,
        'col-sm-3': !oddRow,
        'instafeed-post--small-shift': smallShift,
        'instafeed-post--medium-shift': mediumShift,
        'instafeed-post--large-shift': largeShift,
      }
      if (i % 7 === 0 || i % 7 === 3) {
        posts.push(
          <div key={'separator-' + i} className="col-sm-12 hidden-xs"/>
        );
      }
      if (i % 2 === 0 && i !== 0) {
        posts.push(
          <div key={'separator-sm-' + i} className="col-xs-12 visible-xs"/>
        );
      }
      className = classNames(wrapperClasses, 'col-xs-6', 'instafeed-post');
      posts.push(
        <div key={key} className={className}>
          <a href={post.link} target="_blank" className="instafeed-link">
            <img className="img-responsive" src={post.image}/>
            <span className="instafeed-text">{post.text}</span>
          </a>
        </div>
      );
    }
    return posts;
  },
  // react lifecycle function
  render: function() {
    return (
      <div className="col-md-6 instafeed">
        <h1>INSTAGRAM FEED</h1>
        {this.renderPosts()}
      </div>
    );
  }

});

module.exports = connect(function(state) {
  return {data: state.homepageData.instagramFeed}
})(InstagramFeed);
