var React = require('react');
var ReactDOM = require('react-dom');
// var browserHistory = require('react-router').browserHistory;
// var hashHistory = require('react-router').hashHistory;
var useRouterHistory = require('react-router').useRouterHistory;
var createHashHistory = require('history').createHashHistory;
var createStore = require('redux').createStore;
var Root = require('./Root');
var _ = require('lodash');
var jQuery = require('jquery');
var config = require('./config');

var rootEl = document.getElementById('root');
var store;
var browserSyncBase;
// Use hash location for Github Pages
// but switch to HTML5 history locally.
// var history = process.env.NODE_ENV === 'production' ?
//   hashHistory :
//   browserHistory;
// var history = hashHistory;
var history = useRouterHistory(createHashHistory)({ queryKey: false });
require('whatwg-fetch');
require('es6-promise').polyfill();
global.jQuery = jQuery;

function appReducer(state, action) {
  var actionHandlers = {
    'HOMEPAGE_ARTISTS_DATA_LOADED': function() {
      var ret = _.assign({}, state);
      var data = action.data;
      var artistByGallery = {};
      var artistByVideo = {};
      var artistBySlug = {};
      var galleriesByArtist = {};
      var videosByArtist = {};
      var artistsByProfession = {
        'all': [],
      };
      var artistsBag;
      var galleriesBag;
      data.forEach(function(artist) {
        var slug = artist.slug;
        artist.photogalleries && artist.photogalleries.forEach(
          function(photogallery) {
            artistByGallery[photogallery.id] = slug;
            photogallery.appLink =
              '/' + config.linkSlugs.photogallery + '/' + photogallery.slug;
            galleriesBag = galleriesByArtist[slug] || [];
            galleriesBag.push(photogallery);
            galleriesBag[photogallery.slug] = photogallery
            galleriesByArtist[slug] = galleriesBag;
          }
        );
        artist.videogalleries && artist.videogalleries.forEach(
          function(videogallery) {
            artistByGallery[videogallery.id] = slug;
            videogallery.appLink =
              '/' + config.linkSlugs.videogallery + '/' + videogallery.slug;
            galleriesBag = galleriesByArtist[slug] || [];
            galleriesBag.push(videogallery);
            galleriesBag[videogallery.slug] = videogallery
            galleriesByArtist[slug] = galleriesBag;
          }
        );
        artist.videos && artist.videos.forEach(
          function(video) {
            artistByVideo[video.id] = slug;
            if (video.video) {
              video.appLink =
                '/' + config.linkSlugs.artist + '/' + slug + '/' +
                config.linkSlugs.videos + '/' + video.slug;
            }
            galleriesBag = videosByArtist[slug] || [];
            galleriesBag.push(video);
            galleriesBag[video.slug] = video
            videosByArtist[slug] = galleriesBag;
          }
        );
        artistsByProfession['all'].push(artist);
        artistBySlug[slug] = artist;
        artist.professions && artist.professions.forEach(function(slug) {
          artistsBag = artistsByProfession[slug] || [];
          artistsBag.push(artist);
          artistsByProfession[slug] = artistsBag;
        });
      });
      _.set(ret, 'homepageData.artists', data);
      _.set(ret, 'sharedData.artistByGallery', artistByGallery);
      _.set(ret, 'sharedData.artistByVideo', artistByVideo);
      _.set(ret, 'sharedData.artistsByProfession', artistsByProfession);
      _.set(ret, 'sharedData.galleriesByArtist', galleriesByArtist);
      _.set(ret, 'sharedData.videosByArtist', videosByArtist);
      _.set(ret, 'sharedData.artistBySlug', artistBySlug);
      return ret;
    },
    'HOMEPAGE_POSTS_DATA_LOADED': function() {
      var ret = _.assign({}, state);
      var data = action.data;
      var postBySlug = {};
      data.forEach(function(post) {
        postBySlug[post.slug] = post;
      });
      _.set(ret, 'homepageData.postFeed', action.data);
      _.set(ret, 'sharedData.postBySlug', postBySlug);
      return ret;
    },
    'HOMEPAGE_INSTAGRAM_DATA_LOADED': function() {
      var ret = _.assign({}, state);
      _.set(ret, 'homepageData.instagramFeed', action.data);
      return ret;
    },
    'SHARED_CATEGORIES_DATA_LOADED': function() {
      var ret = _.assign({}, state);
      var data = ret.sharedData.categories.concat(action.data);
      var categoriesByArtist = {};
      var categoriesByGallery = {};
      var videoCategoriesByArtist = {};
      var categoriesByVideo = {};
      var galleriesByArtist = ret.sharedData.galleriesByArtist;
      var videosByArtist = ret.sharedData.videosByArtist;
      data.forEach(function(category, index) {
        category.photogalleries.forEach(function(photogallery) {
          var artistSlug = ret.sharedData.artistByGallery[photogallery.id];
          var categoryObject;
          var galleryDef;
          if (!artistSlug) {
            return;
          }
          galleryDef = galleriesByArtist[artistSlug][photogallery.slug];
          categoryObject = {
            type: category.taxonomy,
            slug: category.slug,
            name: category.name,
            picture: galleryDef.picture,
            width: galleryDef.width,
            ratio: galleryDef.ratio,
            appLink: '/' + config.linkSlugs.photographers + '/' + category.slug,
            index: index,
          };
          _.set(
            categoriesByArtist,
            artistSlug + '.' + category.slug,
            categoryObject
          );
          _.set(
            categoriesByGallery,
            photogallery.slug + '.' + category.slug,
            categoryObject
          );
        });
        category.videogalleries.forEach(function(videogallery) {
          var artistSlug = ret.sharedData.artistByGallery[videogallery.id];
          var categoryObject;
          if (!artistSlug) {
            return;
          }
          categoryObject = {
            type: category.taxonomy,
            slug: category.slug,
            name: category.name,
          };
          _.set(
            categoriesByArtist,
            artistSlug + '.' + category.slug,
            categoryObject
          );
          _.set(
            categoriesByGallery,
            videogallery.slug + '.' + category.slug,
            categoryObject
          );
        });
        category.videos.forEach(function(video) {
          var artistSlug = ret.sharedData.artistByVideo[video.id];
          var categoryObject;
          var videoDef;
          if (!artistSlug) {
            return;
          }
          videoDef = videosByArtist[artistSlug][video.slug];
          categoryObject = {
            type: category.taxonomy,
            slug: category.slug,
            name: category.name,
            picture: videoDef.picture,
            width: videoDef.width,
            ratio: videoDef.ratio,
            index: index,
          };
          _.set(
            videoCategoriesByArtist,
            artistSlug + '.' + category.slug,
            categoryObject
          );
          _.set(
            categoriesByVideo,
            video.slug + '.' + category.slug,
            categoryObject
          );
        });
      });
      _.set(ret, 'sharedData.categories', data);
      _.set(ret, 'sharedData.categoriesByArtist', categoriesByArtist);
      _.set(ret, 'sharedData.categoriesByGallery', categoriesByGallery);
      _.set(ret, 'sharedData.videoCategoriesByArtist', videoCategoriesByArtist);
      _.set(ret, 'sharedData.categoriesByVideo', categoriesByVideo);
      return ret;
    },
    'SHARED_CONTACTFORM_LOADED': function() {
      var ret = _.assign({}, state);
      var data = action.data;
      _.set(ret, 'sharedData.contactForm', data);
      return ret;
    },
    'SHARED_GALLERY_LOADED' : function() {
      var ret = _.assign({}, state);
      var galleriesBySlug = [];
      var data = action.data;
      var imageList;
      data.forEach(function(gallery) {
        imageList = [];
        gallery.gallery.forEach(function(g) {
          g.src.forEach(function(image) {
            imageList.push(image);
          });
        });
        gallery.images = imageList;
        galleriesBySlug[gallery.slug] = gallery;
      });
      _.set(ret, 'sharedData.galleriesBySlug', galleriesBySlug);
      return ret;
    },
    'LIGHTBOX_PHOTO_DATA_LOADED': function() {
      var ret = _.assign({}, state);
      var data = action.data.map(function(photo) {
        photo.appLink = '/' + config.linkSlugs.lightbox + '/' + photo.id;
        return photo;
      });
      _.set(ret, 'sharedData.lightboxData', data);
      return ret;
    },
    'LIGHTBOX_DATA_LOADED': function() {
      var ret = _.assign({}, state);
      var data = action.data;
      _.set(ret, 'sharedData.lightboxImages', data);
      return ret;
    },
    'LIGHTBOX_SWAP_IMAGE': function() {
      var ret = _.assign({}, state);
      var lightboxImages = _.get(state, 'sharedData.lightboxImages') || {};
      var lightboxData = _.get(state, 'sharedData.lightboxData') || {};
      var data = action.data;
      var id = data.id;
      data.appLink = '/' + config.linkSlugs.lightbox + '/' + data.id;
      if (lightboxImages[id]) {
        delete lightboxImages[id];
        lightboxData = lightboxData.filter(function(image) {
          return image.id !== id;
        })
      } else {
        lightboxImages[id] = true;
        lightboxData.push(data);
      }
      _.set(ret, 'sharedData.lightboxImages', lightboxImages);
      _.set(ret, 'sharedData.lightboxData', lightboxData);
      return ret;
    },
    'DEFAULT': function() {
      return state;
    },
  };
  var defaultState = {
    sharedData: {
      categories: [],
      artistByGallery: {},
      categoriesByArtist: {},
      categoriesByGallery: {},
      galleriesByArtist: {},
      artistBySlug: {},
      artistsByProfession: {},
      contactForm: '',
      postBySlug: '',
      galleriesBySlug: [],
      categoriesByVideo: {},
      videoCategoriesByArtist: {},
      artistByVideo: {},
      videosByArtist: {},
      lightboxImages: {},
      lightboxData: [],
    },
    homepageData: {
      artists: {},
      instagramFeed: {},
      postFeed: {},
    },
  };
  var response;
  if (typeof state === 'undefined') {
    return defaultState;
  }
  response = actionHandlers[action.type] || actionHandlers['DEFAULT'];
  return response();
}

store = createStore(appReducer);

ReactDOM.render(<Root history={history} store={store}/>, rootEl);

if (process.env.NODE_ENV === 'development') {
  browserSyncBase = '<script async src="' +
    '//HOST:8080/browser-sync/browser-sync-client.js' +
    '"><\/script>';
  document.write(browserSyncBase.replace('HOST', 'localhost'));
}
