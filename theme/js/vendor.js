/* global FB:false */
window.fbAsyncInit = function() {
  FB.init({
    appId      : 'your-app-id',
    xfbml      : true,
    version    : 'v2.6'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = '//connect.facebook.net/en_US/sdk.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.recaptchaCallback = function() {
  var forms = document.getElementsByTagName('form');
  var pattern = /(^|\s)g-recaptcha(\s|$)/;
  console.log('qq', forms);

  for (var i = 0; i < forms.length; i++) {
    var divs = forms[i].getElementsByTagName('div');

    for (var j = 0; j < divs.length; j++) {
      var sitekey = divs[j].getAttribute('data-sitekey');

      if (divs[j].className && divs[j].className.match(pattern) && sitekey) {
        window.grecaptcha.render(divs[j], {
          'sitekey': sitekey,
          'theme': divs[j].getAttribute('data-theme'),
          'type': divs[j].getAttribute('data-type'),
          'size': divs[j].getAttribute('data-size'),
          'tabindex': divs[j].getAttribute('data-tabindex'),
          'callback': divs[j].getAttribute('data-callback'),
          'expired-callback': divs[j].getAttribute('data-expired-callback')
        });

        break;
      }
    }
  }
}
