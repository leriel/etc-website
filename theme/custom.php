<?php 

function get_attachment_id_from_src($image_src) {
  global $wpdb;
  $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
  $id = $wpdb->get_var($query);
  return $id;
}

add_filter( 'wpcf_type', 'add_show_in_rest_func', 10, 2);
add_filter( 'wpcf_taxonomy_data', 'add_show_in_rest_func', 10, 2);
function add_show_in_rest_func($data, $post_type) {
  $data['show_in_rest'] = true;
  return $data;
}

add_action( 'rest_api_init', 'slug_register_galleryPhoto' );
function slug_register_galleryPhoto() {
  register_rest_field(
    array('photogallery', 'videogallery'),
    'photo',
    array(
      'get_callback'    => 'slug_get_galleryPhoto',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    array('video'),
    'thumbnail',
    array(
      'get_callback'    => 'slug_get_videoThumb',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    array('video'),
    'url',
    array(
      'get_callback'    => 'slug_get_videoURL',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    array('photogallery', 'videogallery'),
    'gallery',
    array(
      'get_callback'    => 'slug_get_galleryGallery',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    array('photogallery', 'videogallery'),
    'artist',
    array(
      'get_callback'    => 'slug_get_artist',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    array('left-hand-category', 'right-hand-category'),
    'photogalleries',
    array(
      'get_callback'    => 'slug_get_photogalleries',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    array('left-hand-category', 'right-hand-category'),
    'videogalleries',
    array(
      'get_callback'    => 'slug_get_videogalleries',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    array('left-hand-category', 'right-hand-category'),
    'videos',
    array(
      'get_callback'    => 'slug_get_videos',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    'artist',
    'photogalleries',
    array(
      'get_callback'    => 'slug_get_photoChildren',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    'artist',
    'bio',
    array(
      'get_callback'    => 'slug_get_bio',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  // register_rest_field(
  //   'artist',
  //   'videogalleries',
  //   array(
  //     'get_callback'    => 'slug_get_videoChildren',
  //     'update_callback' => null,
  //     'schema'          => null,
  //   )
  // );
  register_rest_field(
    'artist',
    'videos',
    array(
      'get_callback'    => 'slug_get_vChildren',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    'artist',
    'profile-picture',
    array(
      'get_callback'    => 'slug_get_profilePic',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    'artist',
    'videos-picture',
    array(
      'get_callback'    => 'slug_get_videoPic',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    'artist',
    'photographer-picture',
    array(
      'get_callback'    => 'slug_get_photographerPic',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    'artist',
    'director-picture',
    array(
      'get_callback'    => 'slug_get_directorPic',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    'artist',
    'animation-picture',
    array(
      'get_callback'    => 'slug_get_animationPic',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    'artist',
    'music-picture',
    array(
      'get_callback'    => 'slug_get_musicPic',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    'artist',
    'professions',
    array(
      'get_callback'    => 'slug_get_artistProfessions',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    array('post', 'blog'),
    'homePicture',
    array(
      'get_callback'    => 'slug_get_postHomePic',
      'update_callback' => null,
      'schema'          => null,
    )
  );
  register_rest_field(
    array('post', 'blog'),
    'artist',
    array(
      'get_callback'    => 'slug_get_artist_info',
      'update_callback' => null,
      'schema'          => null,
    )
  );

}

function slug_get_photogalleries( $object, $field_name, $request ) {
  $ret = array();
  $posts = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'photogallery',
    'orderby' => 'date',
    'order' => 'ASC',
    'tax_query' => array(
      array(
        'taxonomy' => $object['taxonomy'],
        'field' => 'term_id',
        'terms' => $object['id'],
      ),
    ),
  ));
  foreach ($posts as $post) {
    array_push($ret, array(
      'id' => $post->ID,
      'slug' => $post->post_name,
    ));
  }
  return $ret;
}

function slug_get_videos( $object, $field_name, $request ) {
  $ret = array();
  $posts = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'video',
    'orderby' => 'date',
    'order' => 'ASC',
    'tax_query' => array(
      array(
        'taxonomy' => $object['taxonomy'],
        'field' => 'term_id',
        'terms' => $object['id'],
      ),
    ),
  ));
  foreach ($posts as $post) {
    array_push($ret, array(
      'id' => $post->ID,
      'slug' => $post->post_name,
    ));
  }
  return $ret;
}

function slug_get_artistProfessions( $object, $field_name, $request ) {
  $ret = wp_get_post_terms($object['id'], 'profession', array('fields' => 'slugs'));
  return $ret;
}

function slug_get_videogalleries( $object, $field_name, $request ) {
  $ret = array();
  $posts = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'videogallery',
    'tax_query' => array(
      array(
        'taxonomy' => $object['taxonomy'],
        'field' => 'term_id',
        'terms' => $object['id'],
      ),
    ),
  ));
  foreach ($posts as $post) {
    array_push($ret, array(
      'id' => $post->ID,
      'slug' => $post->post_name,
    ));
  }
  return $ret;
}

function slug_get_photoChildren( $object, $field_name, $request ) {
  $ret = array();
  $posts = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'photogallery',
    'meta_key' => ETC_Config::GALLERY_KEY_ARTIST,
    'meta_value' => $object['id'],
    'orderby' => 'date',
    'order' => 'ASC',
    ));
  foreach ($posts as $post) {
    $id = $post->ID;
    $name = $post->post_title;
    $slug = $post->post_name;
    $picture = get_post_meta($id, ETC_Config::GALLERY_KEY_COVER_PHOTO, true);
    $position = get_post_meta($id, ETC_Config::GALLERY_KEY_CROSSHATCH_POSITION, true);
    $width = '';
    $ratio = '';
    if ($picture) {
      $pid = get_attachment_id_from_src($picture);
      $meta = wp_get_attachment_metadata($pid);
      $width = $meta[ETC_Config::MEDIA_META_KEY_WIDTH];
      $height = $meta[ETC_Config::MEDIA_META_KEY_HEIGHT];
      $ratio = $width > 0 ? $height / $width : 0;
    }
    array_push($ret, compact('id', 'picture', 'slug', 'width', 'ratio', 'name', 'position'));
  }
  return $ret;
}

function slug_get_videoChildren( $object, $field_name, $request ) {
  $ret = array();
  $posts = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'videogallery',
    'meta_key' => ETC_Config::GALLERY_KEY_ARTIST,
    'meta_value' => $object['id'],
    ));
  foreach ($posts as $post) {
    $id = $post->ID;
    $name = $post->post_title;
    $slug = $post->post_name;
    $picture = get_post_meta($id, ETC_Config::GALLERY_KEY_COVER_PHOTO, true);
    $position = get_post_meta($id, ETC_Config::GALLERY_KEY_CROSSHATCH_POSITION, true);
    $width = '';
    $ratio = '';
    if ($picture) {
      $pid = get_attachment_id_from_src($picture);
      $meta = wp_get_attachment_metadata($pid);
      $width = $meta[ETC_Config::MEDIA_META_KEY_WIDTH];
      $height = $meta[ETC_Config::MEDIA_META_KEY_HEIGHT];
      $ratio = $width > 0 ? $height / $width : 0;
    }
    array_push($ret, compact('id', 'picture', 'slug', 'width', 'ratio', 'name', 'position'));
  }
  return $ret;
}
function slug_get_vChildren( $object, $field_name, $request ) {
  $ret = array();
  $posts = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'video',
    'meta_key' => ETC_Config::GALLERY_KEY_ARTIST,
    'meta_value' => $object['id'],
    ));
  foreach ($posts as $post) {
    $id = $post->ID;
    $name = $post->post_title;
    $slug = $post->post_name;
    $picture = get_post_meta($id, ETC_Config::VIDEO_META_KEY_THUMBNAIL, true);
    $video = get_post_meta($id, ETC_Config::VIDEO_META_KEY_URL, true);
    $overlay = get_post_meta($id, ETC_Config::VIDEO_META_KEY_OVERLAY, true);
    $src = $picture;
    $width = '';
    $ratio = '';
    if ($picture) {
      $pid = get_attachment_id_from_src($picture);
      $meta = wp_get_attachment_metadata($pid);
      $width = $meta[ETC_Config::MEDIA_META_KEY_WIDTH];
      $height = $meta[ETC_Config::MEDIA_META_KEY_HEIGHT];
      $ratio = $width > 0 ? $height / $width : 0;
    }
    array_push($ret, compact('id', 'picture', 'src', 'video', 'overlay', 'slug', 'width', 'ratio', 'name'));
  }
  return $ret;
}

function slug_get_profilePic( $object, $field_name, $request ) {
  $url = get_post_meta( $object[ 'id' ], ETC_Config::ARTIST_META_KEY_PICTURE, true );
  $pid = get_attachment_id_from_src($url);
  $meta = wp_get_attachment_metadata($pid);
  $imageWidth = $meta[ETC_Config::MEDIA_META_KEY_WIDTH];
  $imageHeight = $meta[ETC_Config::MEDIA_META_KEY_HEIGHT];
  $ratio = $imageHeight / $imageWidth;
  return array(
    'url' => $url,
    'width' => $imageWidth,
    'height' => $imageHeight,
    'ratio' => $ratio,
  );
}

function getMetaPicture( $id, $key ) {
  $url = get_post_meta( $id, $key, true );
  if (!$url) {
    return null;
  }
  $pid = get_attachment_id_from_src($url);
  $meta = wp_get_attachment_metadata($pid);
  $imageWidth = $meta[ETC_Config::MEDIA_META_KEY_WIDTH];
  $imageHeight = $meta[ETC_Config::MEDIA_META_KEY_HEIGHT];
  $ratio = $imageHeight / $imageWidth;
  return array(
    'url' => $url,
    'width' => $imageWidth,
    'height' => $imageHeight,
    'ratio' => $ratio,
  );
}
function slug_get_videoPic( $object, $field_name, $request ) {
  return getMetaPicture($object['id'], ETC_Config::ARTIST_META_KEY_VIDEO_THUMB);
}
function slug_get_photographerPic($object, $field_name, $request) {
  return getMetaPicture($object['id'], ETC_Config::ARTIST_META_KEY_PHOTOGRAPHER_THUMB);
}
function slug_get_directorPic($object, $field_name, $request) {
  return getMetaPicture($object['id'], ETC_Config::ARTIST_META_KEY_DIRECTOR_THUMB);
}
function slug_get_animationPic($object, $field_name, $request) {
  return getMetaPicture($object['id'], ETC_Config::ARTIST_META_KEY_ANIMATOR_THUMB);
}
function slug_get_musicPic($object, $field_name, $request) {
  return getMetaPicture($object['id'], ETC_Config::ARTIST_META_KEY_MUSICIAN_THUMB);
}

function slug_get_bio( $object, $field_name, $request ) {
  //return get_post_meta( $object[ 'id' ], ETC_Config::ARTIST_META_KEY_BIO, true );
  $bio = get_post_meta( $object[ 'id' ], ETC_Config::ARTIST_META_KEY_BIO, true );
  $bioHtml = apply_filters( 'the_content', html_entity_decode( $bio ) );
  return $bioHtml;
}

/**
 * Get the value of the "starship" field
 *
 * @param array $object Details of current post.
 * @param string $field_name Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function slug_get_galleryPhoto( $object, $field_name, $request ) {
  return get_post_meta( $object[ 'id' ], ETC_Config::GALLERY_KEY_COVER_PHOTO, true );
}
function slug_get_galleryGallery( $object, $field_name, $request ) {
  return getGalleryGalleries($object[ 'id' ]);
}
function getGalleryGalleries($pid) {
  $galleries = get_post_galleries( $pid, false );
  if (isset($galleries) && count($galleries) > 0) {
    return $galleries;
  }
  $p = get_post($pid);
  $content = $p->post_content;
  $pattern = '|<img.*?wp-image-([0-9]*).*?src="([^"]*)".*?/?>|';
  $content = preg_match_all($pattern, $content, $matches);
  if (!isset($matches) || !isset($matches[1]) || count($matches[1]) <= 0) {
    return array();
  }
  $photoIds = $matches[1];
  $gallery = array(array('src' => array()));
  for ($j = count($photoIds) - 1; $j >= 0; $j--) {
    $id = $photoIds[$j];
    $meta = wp_get_attachment_metadata($id);
    $width = $meta[ETC_Config::MEDIA_META_KEY_WIDTH];
    $height = $meta[ETC_Config::MEDIA_META_KEY_HEIGHT];
    $ratio = $width > 0 ? $height / $width : 0;
    $src = wp_get_attachment_image_src($id, 'full')[0];
    $picture = $src;
    $video = get_post_meta($id, ETC_Config::MEDIA_META_KEY_VIDEO, true);
    array_push($gallery[0]['src'], compact('id', 'src', 'picture', 'video', 'width', 'ratio'));
  }
  return $gallery;
}
function slug_get_artist( $object, $field_name, $request ) {
  $belongs = get_post_meta( $object[ 'id' ], ETC_Config::GALLERY_KEY_ARTIST, true );
  return $belongs;
}
function slug_get_artist_info( $object, $field_name, $request ) {
  $belongs = get_post_meta( $object[ 'id' ], ETC_Config::GALLERY_KEY_ARTIST, true );
  if (!$belongs) {
    return null;
  }
  $name = get_the_title($belongs);
  return array(
    'id' => $belongs,
    'name' => $name
  );
}
function slug_get_postHomePic( $object, $field_name, $request ) {
  return get_post_meta( $object[ 'id' ], ETC_Config::POST_META_KEY_FRONTIMAGE, true );
}
function slug_get_videoThumb( $object, $field_name, $request ) {
  return get_post_meta( $object[ 'id' ], ETC_Config::VIDEO_META_KEY_THUMBNAIL, true );
}
function slug_get_videoURL( $object, $field_name, $request ) {
  return get_post_meta( $object[ 'id' ], ETC_Config::VIDEO_META_KEY_URL, true );
}


// add_filter('post_gallery', 'galleryJSON', 10, 2);
add_filter('get_post_galleries', 'gallery_addVideo', 10, 2);

function gallery_addVideo($galleries, $post) {
  for ($i = count($galleries) - 1; $i >= 0; $i--) {
    $gallery = $galleries[$i];
    $ids = explode(',', $gallery['ids']);
    for ($j = count($ids) - 1; $j >= 0; $j--) {
      $id = $ids[$j];
      $meta = wp_get_attachment_metadata($id);
      $width = $meta[ETC_Config::MEDIA_META_KEY_WIDTH];
      $height = $meta[ETC_Config::MEDIA_META_KEY_HEIGHT];
      $ratio = $width > 0 ? $height / $width : 0;
      // $src = $gallery['src'][$j];
      $src = wp_get_attachment_image_src($id, 'full')[0];
      $picture = $src;
      $video = get_post_meta($id, ETC_Config::MEDIA_META_KEY_VIDEO, true);
      $gallery['src'][$j] = compact('id', 'src', 'picture', 'video', 'width', 'ratio');
    }
    $galleries[$i] = $gallery;
  }
  return $galleries;
}

function galleryJSON($string,$attr){

  $ret = array();
  $posts = get_posts(array(
    'include' => $attr['ids'],
    'post_type' => 'attachment'
    ));
  foreach($posts as $imagePost){
    $id = $imagePost->ID;
    $small = wp_get_attachment_image_src($id, 'small')[0];
    $medium = wp_get_attachment_image_src($id, 'medium')[0];
    $large = wp_get_attachment_image_src($id, 'large')[0];
    $extralarge = wp_get_attachment_image_src($id, 'extralarge')[0];
    $video = get_post_meta($id, ETC_Config::MEDIA_META_KEY_VIDEO, true);
    array_push($ret, compact('id', 'small', 'medium', 'large', 'extralarge', 'video'));
  }
  return json_encode($ret);
}

add_action( 'generate_rewrite_rules', 'roots_add_rewrites' );

function roots_add_rewrites($content) {
  $theme_name = next( explode( '/themes/', get_stylesheet_directory() ) );
  global $wp_rewrite;
  $roots_new_non_wp_rules = array(
    'styles/(.*)' => 'wp-content/themes/' . $theme_name . '/styles/$1',
    'js/(.*)'  => 'wp-content/themes/' . $theme_name . '/js/$1',
    'images/(.*)' => 'wp-content/themes/' . $theme_name . '/images/$1',
    'fonts/(.*)' => 'wp-content/themes/' . $theme_name . '/fonts/$1',
    'favicon.ico' => 'wp-content/themes/' . $theme_name . '/favicon.ico',
  );
  $new_wp_rules = array(
    // '^api/?$' => 'index.php?rest_route=/',
    '^api/(.*)?$' => 'index.php?rest_route=/wp/v2/$matches[1]',
    '^photographers/(.*)?$' => 'index.php?pagename=photographers',
  );
  $wp_rewrite->non_wp_rules += $roots_new_non_wp_rules;
  $wp_rewrite->rules = $new_wp_rules + $wp_rewrite->rules;
}

function getInstagramFeed($data) {
  $limit = $data->get_param('limit');
  // $feed = file_get_contents('https://api.instagram.com/v1/users/2671617422/media/recent?access_token=3220597561.1677ed0.0e8fc61ac79d47739a208e61702f7162&count=' . $limit);
  $feed = file_get_contents(dirname(__FILE__) . '/instagram.json');
  $feed = json_decode($feed);
  // $ret = array();
  // if (!$feed || !isset($feed->data)) {
  //   return array();
  // }
  // foreach ($feed->data as $post) {
  //   $image = null;
  //   $text = null;
  //   if (isset($post->images) && isset($post->images->low_resolution)) {
  //     $image = $post->images->low_resolution->url;
  //   }
  //   if (isset($post->caption)) {
  //     $text = $post->caption->text;
  //   }
  //   array_push($ret, array(
  //     'link' => $post->link,
  //     'image' => $image,
  //     'text' => $text,
  //   ));
  // }
  return $feed;
}

function getContactForm() {
  $contactFormHtml = do_shortcode('[contact-form-7 id="165" title="SHARE"]');
  return preg_replace('/\n */i', '', $contactFormHtml);
  return $contactFormHtml;
}

function getLightbox() {
  $ret = [];
  if (!$_COOKIE || !isset($_COOKIE['lb'])) {
    return $ret;
  }
  $photos = stripslashes($_COOKIE['lb']);
  $photos = json_decode($photos);
  foreach ($photos as $id => $value) {
    $meta = wp_get_attachment_metadata($id);
    $width = $meta[ETC_Config::MEDIA_META_KEY_WIDTH];
    $height = $meta[ETC_Config::MEDIA_META_KEY_HEIGHT];
    $ratio = $width > 0 ? $height / $width : 0;
    // $video = get_post_meta($id, ETC_Config::MEDIA_META_KEY_VIDEO, true);
    $picture = wp_get_attachment_image_src($id, 'full')[0];
    array_push($ret, compact('id', 'picture', 'width', 'height', 'ratio'));
  }
  return $ret;
}

function getPhotogalleries($request) {
  $ret = array();
  $posts = get_posts(array(
    'posts_per_page' => -1,
    'post_type' => 'photogallery',
  ));
  foreach ($posts as $post) {
    $photo = get_post_meta( $post->ID, ETC_Config::GALLERY_KEY_COVER_PHOTO, true);
    $artist = get_post_meta( $post->ID, ETC_Config::GALLERY_KEY_ARTIST, true );
    $gallery = getGalleryGalleries($post->ID);
    array_push($ret, array(
      'id' => $post->ID,
      'slug' => $post->post_name,
      'photo' => $photo,
      'artist' => $artist,
      'gallery' => $gallery,
      'title' => array('rendered' => $post->post_title),
    ));
  }
  return $ret;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'wp/v2', '/instagram', array(
    'methods' => 'GET',
    'callback' => 'getInstagramFeed',
    'args' => array(
      'limit' => array(
        'default' => 14,
        'sanitize_callback' => 'absint',
      ),
    ),
  ) );
  register_rest_route( 'wp/v2', '/contactform', array(
    'methods' => 'GET',
    'callback' => 'getContactForm',
  ) );
  register_rest_route( 'wp/v2', '/allgalleries', array(
    'methods' => 'GET',
    'callback' => 'getPhotogalleries',
  ) );
  register_rest_route( 'wp/v2', '/lightbox', array(
    'methods' => 'GET',
    'callback' => 'getLightbox',
  ) );
  register_rest_route( 'wp/v2', '/download/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'downloadImage',
    'args' => array(
      'id' => array(
        'validate_callback' => function($param, $request, $key) {
          return is_numeric($param);
        }
      ),
    ),
  ) );
  register_rest_route( 'wp/v2', '/pdf', array(
    'methods' => 'GET',
    'callback' => 'getPDF',
  ) );
} );


function sort_views_column( $vars ) 
{
    if ( isset( $vars['orderby'] ) && 'artist' == $vars['orderby'] ) {
        $vars = array_merge( $vars, array(
            'meta_key' => ETC_Config::GALLERY_KEY_ARTIST, //Custom field key
            'orderby' => 'meta_value_num') //Custom field value (number)
        );
    }
    return $vars;
}
add_filter( 'request', 'sort_views_column' );

function addArtistColumn($cols) {
  $cols['artist'] = __('Artist', 'user-column');
  return $cols;
}

function makeArtistSortable($cols) {
  $cols['artist'] = 'artist';
  return $cols;
}

function getArtistValue($column, $pid) {
  if( $column == 'artist' ) {
    $artistId = get_post_meta($pid, ETC_Config::GALLERY_KEY_ARTIST, true);
    if ($artistId) {
      echo get_the_title($artistId);
    }
  }
}

add_filter('manage_edit-photogallery_columns', 'addArtistColumn', 15, 1);
add_filter('manage_edit-photogallery_sortable_columns', 'makeArtistSortable', 15, 1);
add_action('manage_photogallery_posts_custom_column', 'getArtistValue', 15, 3);

add_filter('manage_edit-videogallery_columns', 'addArtistColumn', 15, 1);
add_filter('manage_edit-videogallery_sortable_columns', 'makeArtistSortable', 15, 1);
add_action('manage_videogallery_posts_custom_column', 'getArtistValue', 15, 3);

add_filter('manage_edit-video_columns', 'addArtistColumn', 15, 1);
add_filter('manage_edit-video_sortable_columns', 'makeArtistSortable', 15, 1);
add_action('manage_video_posts_custom_column', 'getArtistValue', 15, 3);

add_filter('manage_edit-post_columns', 'addArtistColumn', 15, 1);
add_filter('manage_edit-post_sortable_columns', 'makeArtistSortable', 15, 1);
add_action('manage_post_posts_custom_column', 'getArtistValue', 15, 3);

function custom_excerpt_length( $length ) {
  return 40;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function my_rest_prepare_post( $data, $post, $request ) {
  $_data = $data->data;
  $params = $request->get_params();
  unset( $_data['content'] );
  $data->data = $_data;
  return $data;
}
add_filter( 'rest_prepare_photogallery', 'my_rest_prepare_post', 10, 3 );

function downloadImage($data) {
  $id = $data['id'];
  $watermark_url = get_template_directory() . '/images/watermark.png';
  $image_url = wp_get_attachment_image_src($id, 'full');
  $image_url = $_SERVER["DOCUMENT_ROOT"] . parse_url($image_url[0], PHP_URL_PATH);
  $file_name = basename($image_url);
  $watermark = imagecreatefrompng($watermark_url);
  $source = imagecreatefromjpeg($image_url);

  $space_right = 0;
  $space_bottom = 0;
  $sx = imagesx($watermark);
  $sy = imagesy($watermark);

  imagecopy($source, $watermark, imagesx($source) - $sx - $space_right, imagesy($source) - $sy - $space_bottom, 0, 0, imagesx($watermark), imagesy($watermark));

  header('Content-Description: File Transfer');
  header('Content-Type: application/octet-stream');
  header('Content-Disposition: attachment; filename="'. $file_name .'"');
  header('Content-Transfer-Encoding: binary');
  header('Connection: Keep-Alive');
  header('Expires: 0');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');
  //header('Content-Length: ' . filesize($file));

  imagejpeg($source);
  imagedestroy($source);                  
  exit();
}

function getPDF() {
  include (get_template_directory() . '/vendor/mpdf60/mpdf.php');

  $mpdf = new mPDF('','A4-L', 0, '', 0, 0, 0, 0, 0, 0);
  $mpdf->autoScriptToLang = true;
  $mpdf->SetAuthor('ETC Creative');
  $mpdf->SetTitle('Lightbox Images');
  $mpdf->SetDisplayMode('fullpage');

  //<a href="' . get_site_url() . '">WWW.HOLLENDERX2.COM</a>

  $html = '<style>
    .logo-pdf {padding-top: 80mm; width: 88mm; height: 45mm; margin-left: auto; margin-right: auto;}
    .footer-pdf {padding-top: 60mm; font-size: 12pt; font-family: "Proxima"; text-align: center; color:rgb(0,0,0)}
    a {color:rgb(0,0,0); text-decoration:none}
    </style>
    <div class="logo-pdf"><img src="' . get_template_directory() . '/images/logo-pdf.jpg"></div>
    <div class="footer-pdf">NYC: 347-409-9973  CHI: 847-563.8178  ///  <a href="mailto:erica@etccreativeinc.com">erica@etccreativeinc.com</a> /// <a href="thisisetccreative.com">thisisetccreative.com</a></div>';

  $mpdf->WriteHTML($html);

  $pix = getLightbox();

  foreach ($pix as $pic) {
    $src = $pic['picture'];
    $w = floor($pic['width'] * 0.264583333);
    $h = floor($pic['height'] * 0.264583333);
    $ratio = $w / $h;

    $h = 160;
    $w = round($h * $ratio);
    if ($w > 297 - 40) {
        $w = 297 - 40;
        $h = $w / $ratio;
    }
    $y = (210 - $h - 10) / 2;
    $x = floor((297 - $w) / 2);
    $artist = get_the_title(wpcf_pr_post_get_belongs(wp_get_post_parent_id($pic['id']), 'artist'));

    $mpdf->AddPage();
    //$mpdf->Image($src, $x, $y, $w, $h, 'jpg', '', true, false);
    $artist_html = '<style>
      div {width: ' . $w .'mm; height: ' . $h . 'mm; padding-top: ' . $y . 'mm; margin-left: auto; margin-right:auto}
      p {font-family: "Proxima"; font-size: 12pt; color: rgb(0,0,0); margin-top: 2mm; padding-left: ' . $x .'mm}
      </style>
      <div><img src="' . $src .  '"></div><p>photo: ' . $artist . '</p>';
    $mpdf->WriteHTML($artist_html);
    //echo $pic['picture'] . ' width - ' . $pic['width'] . ' height - ' . $pic['height'] .' artist - ' . get_the_title(wpcf_pr_post_get_belongs(wp_get_post_parent_id($pic['id']), 'artist')) . "\n";
  }

  $mpdf->Output('ETC-Creative.pdf', 'D');
  //$mpdf->Output('etc_creative.pdf', 'D');

  unset ($html);
  unset ($mpdf);
  die();
}

?>
