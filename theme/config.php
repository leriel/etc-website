<?php

class ETC_Config {
  const MEDIA_META_KEY_VIDEO = 'wpcf-video-link';
  const MEDIA_META_KEY_WIDTH = 'width';
  const MEDIA_META_KEY_HEIGHT = 'height';

  const VIDEO_META_KEY_URL = 'wpcf-video-url';
  const VIDEO_META_KEY_THUMBNAIL = 'wpcf-thumbnail';
  const VIDEO_META_KEY_OVERLAY = 'wpcf-overlay-image';
  
  const POST_META_KEY_FRONTIMAGE = 'wpcf-frontpage-image';

  const GALLERY_KEY_ARTIST = '_wpcf_belongs_artist_id';
  const GALLERY_KEY_COVER_PHOTO = 'wpcf-photo';
  const GALLERY_KEY_CROSSHATCH_POSITION = 'wpcf-cross-hatch-position';

  const ARTIST_META_KEY_PICTURE = 'wpcf-profile-picture';
  const ARTIST_META_KEY_BIO = 'wpcf-bio';
  const ARTIST_META_KEY_VIDEO_THUMB = 'wpcf-video-profile-picture';
  const ARTIST_META_KEY_PHOTOGRAPHER_THUMB = 'wpcf-photographer-promo';
  const ARTIST_META_KEY_DIRECTOR_THUMB = 'wpcf-director-promo';
  const ARTIST_META_KEY_ANIMATOR_THUMB = 'wpcf-animation-promo';
  const ARTIST_META_KEY_MUSICIAN_THUMB = 'wpcf-music-promo';
}
