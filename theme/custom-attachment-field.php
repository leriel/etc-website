<?php 

function media_getVimeoLink($formFields, $post) {
  $fieldKey = ETC_Config::MEDIA_META_KEY_VIDEO;
  $formFields[$fieldKey]['label'] = __('Video link');
  $formFields[$fieldKey]['input'] = 'text';
  $formFields[$fieldKey]['value'] = get_post_meta($post->ID, $fieldKey, true);
  return $formFields;
}

add_filter('attachment_fields_to_edit', 'media_getVimeoLink', 10, 2);

function media_setVimeoLink($post, $attachment) {
  $fieldKey = ETC_Config::MEDIA_META_KEY_VIDEO;
  if(!isset($attachment[$fieldKey])) {
    return;
  }
  if(trim($attachment[$fieldKey]) == '') {
    // adding our custom error
    $post['errors']['linked-subgallery']['errors'][] = __('Error text here.');
  } else {
    $meta = get_post_meta( $post['ID'] );
    $res = update_post_meta($post['ID'], $fieldKey, $attachment[$fieldKey]);
  }
  return $post;
}

add_filter('attachment_fields_to_save','media_setVimeoLink', 10, 2);

?>
