<?php 
  if (file_exists("wp-content/plugins/types/admin.php")) {
    include_once "wp-content/plugins/types/admin.php";
    include_once "wp-content/plugins/types/includes/import-export.php";    
  } else {
    include_once "wp-content/plugins/types/library/toolset/types/admin.php";
    include_once "wp-content/plugins/types/library/toolset/types/includes/import-export.php";    
  }
  function onFail($msg = '') {
    die('fail ' . $msg);
  }
  $fileName = $args[0];
  if (!file_exists($fileName)) {
    onFail('no file: ' . $fileName);
  }
  $content = @file_get_contents($fileName);
  if (!$content) {
    onFail('no content');
  }
  var_dump( wpcf_admin_import_data($content, false, ''));
?>